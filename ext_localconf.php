<?php
defined('TYPO3') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
         'Churchpersreg',
         'Persoon',
         [
             \Parousia\Churchpersreg\Controller\PersoonController::Class => 'searchForm,zoektips,detail,adressenlijst,ledenlijst,selectedList,save,edit,delete,email,sendEmail,export,contactInfoReg,contactInfoSave,deleteArchive,requestSend,saveRequest,deleteRequest,selectedListAanvragen,detailaanvraag,searchAanvragen,saveAanvraag,deleteAanvraag,selectedListUpdatelogs,detailUpdatelog,searchUpdatelog',
         ],
         // non-cacheable actions
		 [
             \Parousia\Churchpersreg\Controller\PersoonController::Class => 'searchForm,zoektips,detail,adressenlijst,ledenlijst,selectedList,save,edit,delete,email,sendEmail,export,contactInfoReg,contactInfoSave,deleteArchive,requestSend,saveRequest,deleteRequest,selectedListAanvragen,detailaanvraag,searchAanvragen,saveAanvraag,deleteAanvraag,selectedListUpdatelogs,detailUpdatelog,searchUpdatelog',
         ],
     );
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['cleanteamtoekenning'] = \Parousia\Churchpersreg\Hooks\cleanteamtoekenning::class .'::processRequest';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['selectperson'] = \Parousia\Churchpersreg\Hooks\selectperson::class .'::processRequest';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['zoekpostcode'] = \Parousia\Churchpersreg\Hooks\zoekpostcode::class .'::processRequest';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['filehandling'] = \Parousia\Churchpersreg\Hooks\filehandling::class .'::processRequest';
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['templateRootPaths'][701] = 'EXT:churchpersreg/Resources/Private/Templates/Email';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['changepersbyowner'] = \Parousia\Churchpersreg\Hooks\changepersbyowner::class .'::processRequest';



