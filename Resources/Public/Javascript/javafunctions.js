$ = jQuery.noConflict();
function IsDatum(str)
{	if (str.length>0 && str!="00-00-0000")
	{	
		if (/^(0?[1-9]|[1-2]\d|3[0-1])\-(0?[1-9]|1[0-2])\-(19|20)\d{2}$/.test(str))
		{
			var sd = str.split("-");
			var year=sd[2];
			if (sd[1]==2 && sd[0]>(((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 ))return false;
		}
		else return false;
	}
	return true;
}

function FormatDate(d)
{
	// maak datum formaat dd-mm-jjjj
	//alert("inpt date:"+d.val());
	var str=$.trim(d.val()).toLowerCase();
	if (str.length==0)return "";
	var re= /[/\s\.-]+/g
	str=str.replace(re,"-");
	re=/-+/g
	str=str.replace(re,"-");
	re=/([a-z]+)(\d+)/g
	str=str.replace(re,"$1-$2"); // splits tekst en getallen
	re=/(\d+)([a-z]+)/g
	str=str.replace(re,"$1-$2"); // splits tekst en getallen
	var sd = str.split("-");
	if (sd.length==1)
	{	if (!isNaN(sd[0]) && str.length>4)	str=str.substr(0,2)+"-"+str.substr(2,2)+"-"+str.substr(4);else str="01-01-"+sd[0];
		sd=str.split("-");
	}
	if (sd.length==2){str="01-"+sd[0]+"-"+sd[1];sd=str.split("-");}
	// vertaal eventuele maandnaam:
	if (isNaN(sd[1]))
	{/*	var mm={"jan":"1","feb":"2","maa":"3","apr":"4","mei":"5","jun":"6","jul":"7","aug":"8","sep":"9","okt":"10","oct":"10","nov":"11","dec":"12"};
		var mn=mm[sd[1].substr(0,3)]; */
		var mm={"ja":"01","f":"02","ma":"03","ap":"04","me":"05","jun":"06","jul":"07","au":"08","s":"09","o":"10","n":"11","d":"12"};
		var mn=mm[sd[1].substr(0,3)];
		if (!mn) mn=mm[sd[1].substr(0,2)];
		if (!mn) mn=mm[sd[1].substr(0,1)];
		if (mn)	sd[1]=mn; else sd[1]="01";
	}
	if (isNaN(sd[0])) sd[0]="01";
	if (isNaN(sd[1])) sd[1]="01";
	if (!isNaN(sd[2])&&sd[2].length<3)
	{	if (sd[2].length<2) sd[2]="0"+sd[2];
		if (sd[2].length<2) sd[2]="0"+sd[2];
		d=new Date();
		yr=(d.getFullYear()%100)+1;
		if (sd[2]>yr)sd[2]="19"+sd[2];else sd[2]="20"+sd[2];
	}
	if (sd[1].length<2)	{sd[1]='0'+sd[1];}
	if (sd[0].length<2)	{sd[0]='0'+sd[0];}
	str=sd[0]+"-"+sd[1]+"-"+sd[2];
	//alert ("nieuwe datum:"+str);
	if (!IsDatum(str))
	{	alert("Ongeldige datum!");
		return false;
	}
	var gd= str.split('-');
	mydat= gd[2]+gd[1]+gd[0];
	now= new Date();
	now.setDate(now.getDate()+30);
	nu=now.getFullYear().toString();
	curr = nu.concat(("0"+(now.getMonth()+1).toString()).slice(-2),("0"+now.getDate().toString()).slice(-2));
	//alert ("gebdat:"+gebdat+"; now:"+curr);
	if (mydat> curr) 
	{
		alert(str+" te ver in de toekomst");
		return false;
	}

	return str;
}