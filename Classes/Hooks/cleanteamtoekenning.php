<?php
namespace Parousia\Churchpersreg\Hooks;

ini_set("display_errors",1);
ini_set("log_errors",1);
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;

class cleanteamtoekenning 
{
	protected $db;

/**
 * @param ServerRequestInterface $request
 * @param ResponseInterface $response
 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		/*
		* file for ajax to update person idperson to participate in cleanteams or not
		 * Created on 2 June 2017
		 * Post parameters: 
		 	idperson,
			cleanteam,
		 */
	    
		//$person_id=69;
		$response = GeneralUtility::makeInstance(Response::class);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin cleanteammeedoen: "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');

		$clnrequired='kan meedoen';
		$aParms=$request->getParsedBody(); 
	    //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin cleanteammeedoen:".http_build_query($aParms,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');

		if (isset($aParms["cleanteam"]) and $aParms["cleanteam"]=="false")$clnrequired="kan niet meedoen";
		if (isset($aParms["idperson"]))$person_id=$aParms["idperson"];
		else die("You are not privileged to perform this action");

		
		// bepalen van person-id bij opgegeven zoeknaam (voornaam + achternaam, b.v. partner, vader
		// person-id moet ongelijk eigen $person_id zijn, indien ingevuld.
		// bij fout met lege id en foutmelding in self::$ErrMsg)
		$fieldarray['cleanteam']=$clnrequired;
	
		// get old 'cleanteam' field:
		churchpersreg_div::connectdb($this->db);
		$query='select cleanteam,bezoeker,geboortedatum,AES_DECRYPT(id_adres,@password) as id_adres from persoon where uid="'.$person_id.'"';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": cleanteam select person:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
		$result=$this->db->query($query) or die("Can't perform Query");	
		$row=$result->fetch_array(MYSQLI_ASSOC);
		$cleanteam=$row['cleanteam'];
		$bezoeker=$row['bezoeker'];
		$geboortedatum=$row['geboortedatum'];
		$id_adres=$row['id_adres'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": cleanteammeedoen nu: adresid:$id_adres ; ".$cleanteam."; adreslijst:".$bezoeker."; geboortedatum:".$geboortedatum."; req:".$clnrequired."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
	
		if ($cleanteam=='kan niet meedoen' and $clnrequired=='kan meedoen' and $bezoeker=="is bezoeker")
		{
			// update person:
			$this->db->query('update persoon set cleanteam="'.$clnrequired.'" where uid="'.$person_id.'"');
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": cleanteammeedoen persoon toevoegen aan cleanteams:"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
			$this->AddtoCleanteam($person_id,$geboortedatum,$id_adres);
		}
		else if ($cleanteam=='kan meedoen' and ($clnrequired=='kan niet meedoen' or $bezoeker=="is geen bezoeker"))
		{
			// verwijder ook cleanteam-taakbekleding:
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": cleanteammeedoen verwijderen:".$cleanteam."; adreslijst:$bezoeker; geboortedatum:$geboortedatum; req:$clnrequired"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
	
			$this->db->query('delete from taakbekleding where id_persoon="'.$person_id.'" and id_parent in (select uid from taak where omschrijving like \'cleanteam%\')');
			// update person:
			$this->db->query('update persoon set cleanteam="'.$clnrequired.'" where uid="'.$person_id.'"');
		}
		// read new sets of tasks:
/*		$query='SELECT DISTINCT concat(coalesce(tkn.taken,""),if(tkn.taken is null,"",if(ldr.leider is null,"",", ")),coalesce(ldr.leider,"")) as taken '. 
		'FROM (select group_concat(concat("leider ",omschrijving) separator ", ") as leider from bediening b where find_in_set("'.$person_id.'",id_bedieningsleider) ) as ldr,'.
		'persoon as tp '.
		'left join (select group_concat(tk.omschrijving order by tk.omschrijving asc separator ", ") as taken,med.id_persoon from taakbekleding med left join taak tk on (tk.uid = med.id_parent) where med.id_persoon="'.$person_id.'") as tkn on (tkn.id_persoon=tp.uid) '.
		'where tp.uid="'.$person_id.'"'; */

		$query='SELECT DISTINCT concat(coalesce(tkn.taken,""),if(tkn.taken is null,"",if(ldr.leider is null,"",",")),coalesce(ldr.leider,"")) as taken '. 
		'FROM (select group_concat(b.uid,if(b.soort="afdeling","_a#","_b#"),concat("leider ",omschrijving) separator ",") as leider from bediening b where find_in_set("'.$person_id.'",id_bedieningsleider) ) as ldr,'.
		'persoon as tp '.
		'left join (select group_concat(med.uid,"_m#",tk.omschrijving order by tk.omschrijving asc separator ",") as taken,med.id_persoon,med.uid from taakbekleding med left join taak tk on (tk.uid = med.id_parent) where med.id_persoon="'.$person_id.'") as tkn on (tkn.id_persoon=tp.uid) '.
		'where tp.uid="'.$person_id.'"';
		
//		$query="SELECT group_concat(omschrijving separator ', ') as taken from taak as ta, taakbekleding as med where med.id_persoon='".$person_id."' and med.id_parent=ta.uid";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": cleanteammeedoen takenquery:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
	
		$result=$this->db->query($query);
		$row=$result->fetch_array(MYSQLI_ASSOC);
		//exploded taken:
		$tasks="";
		if (!empty($row['taken']))
		{
			$aTaken=explode(",",$row['taken']);
			$taken=array();
			$taken=array_map(function($taak){return explode('#',$taak);},$aTaken);
			$taken= array_column($taken,1,0);
			foreach ($taken as $ministryid => $description)
			{
				$tasks.='<span class="taskselect btn btn-link mr-1 p-0" id="'.$ministryid.'_drop">'.$description.'</span>';
			}
			
		}

		//$data= array('status'=>'success','message'=>'','data'=>$row['taken']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": cleanteammeedoen return taken:".$tasks."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
//		$response->getBody()->write($row['taken']);
		$response->getBody()->write($tasks);
		return $response;
	}
	 
	function AddtoCleanteam($person_id,$geboortedatum,$id_adres)
	{
		// add person to cleanteam:
		$jaaroud=(date('Y')-79).'-09-01';
		$jaarjong=(date('Y')-7).'-09-01';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": AddtoCleanteam geboortedatum:$geboortedatum; jaaroud:$jaaroud; jaarjong:$jaarjong; ".($geboortedatum>$jaaroud?"niet te oud":"te oud")."; ".($geboortedatum<=$jaarjong?"niet te jong":"te jong") ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log'); 
		if (($geboortedatum>$jaaroud and $geboortedatum<=$jaarjong) or empty($geboortedatum) or $geboortedatum=='0000-00-00')
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."AddtoCleanteam get pc of $id_adres"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
			$cleanid1='';
			$cleanid2='';
			$pc1='';
			$pc2='';

			// get postal code:
			$result=$this->db->query('select postcode,lower(land) as land from adres where uid='.$id_adres);
			$adr=$result->fetch_array(MYSQLI_ASSOC);
			$pc=$adr['postcode'];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'AddtoCleanteam pc: '.$pc."; land:".$adr['land']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');

			if (empty($adr['land']) or strtolower($adr['land'])=='nederland')
			{
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'postcode nieuw persoon: '.$pc."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
				// search corresponding cleanteam:
				$result=$this->db->query("select t.uid,a.postcode from taakbekleding tb, taak t,persoon p ,adres a where ".
				"AES_DECRYPT(p.id_adres,@password)=a.uid and a.deleted=0 and p.deleted=0 
				and tb.id_parent=t.uid and tb.id_parent=t.uid and t.omschrijving like 'cleanteam%' and p.uid=tb.id_persoon and 
				a.postcode >='".$pc."' and p.uid<>".$person_id." order by postcode asc");
				$row1=$result->fetch_array(MYSQLI_ASSOC);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'cleanteam1 nieuw persoon: '.$adr['postcode'].'-'.$row1['uid']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
				
				if (!empty($row1)) {$cleanid1=$row1['uid'];$pc1=$row1['postcode'];}
				$result=$this->db->query("select t.uid,a.postcode from taakbekleding tb, taak t,persoon p ,adres a".
				" where AES_DECRYPT(p.id_adres,@password)=a.uid and a.deleted=0 and p.deleted=0 
				and tb.id_parent=t.uid and tb.id_parent=t.uid and t.omschrijving like 'cleanteam%' and p.uid=tb.id_persoon and 
				a.postcode <='".$pc."' and p.uid<>".$person_id." order by postcode desc");
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'cleanteam2 error: '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
				$row2=array();
				if ($result->num_rows>0)
				{
					$row2=$result->fetch_array(MYSQLI_ASSOC);
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'cleanteam2 nieuw persoon: '.$row2['postcode'].'-'.$row2['uid']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
					if (!empty($row2)) {$cleanid2=$row2['uid'];$pc2=$row2['postcode'];}
				}
				if (empty($pc2)) 
				{
					if (!empty($pc1)) $cleanid=$cleanid1; 
					else return false;
				}
				else
				{	if (empty($pc1)) $cleanid=$cleanid2;
					else
					{
						if ($cleanid1==$cleanid2) $cleanid=$cleanid1;
						else
						{
							// determine which postalcode nearest person's postal code:
							$npc=intval(substr($pc,0,4))*676 +(ord(substr($pc,5,1))-64)*26 +ord(substr($pc,6,1))-64;
							$npc1=intval(substr($pc1,0,4))*676 +(ord(substr($pc1,5,1))-64)*26 +ord(substr($pc1,6,1))-64;
							$npc2=intval(substr($pc2,0,4))*676 +(ord(substr($pc2,5,1))-64)*26 +ord(substr($pc2,6,1))-64;
							if (($npc - $npc2)<($npc1-$npc))$cleanid=$cleanid2; else $cleanid=$cleanid1;
/*						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'cleanteam nieuw persoon: pc='.$pc.'('.$npc.'); pc1='.$pc1.'('.$npc1
						.'); pc2='.$pc2.'('.$npc2.'); cleanid1='.$cleanid1.'; cleanid2='.$cleanid2.'; cleanid='.$cleanid
						."\r\n",3,'cleanteammeedoen.log'); */
						}
					}
					
				}
			//	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'cleanid nieuw persoon: '.$cleanid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchadmin/pi3/cleanteammeedoen.log');
				// add person to found cleanteam:
				$query='insert into taakbekleding (id_parent,id_persoon,datum_start,opmerkingen) values ('.$cleanid.','.$person_id.',"'.date('Y-m-d').'","")';
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'inserting cleanteam : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchadmin/pi3/cleanteammeedoen.log');
				$this->db->query($query);
				if (!empty($this->db->error)) 
				{	
					error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'error inserting cleanteam : '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/churchpersreg/Classes/Hooks/cleanteammeedoen.log');
				}
			}
		}
		return true;
	}
}

 

