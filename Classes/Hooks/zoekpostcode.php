<?php
namespace Parousia\Churchpersreg\Hooks;

ini_set("display_errors",1);
ini_set("log_errors",1);
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;

/*
* class for ajax to get personid and name for searchname and required role (partner, farther, mother) with person idperson
*/

class zoekpostcode 
{

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
	 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		$response = GeneralUtility::makeInstance(Response::class);

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin zoekpostcode: ".$_SERVER['DOCUMENT_ROOT']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin zoekpostcode na post:".count($POST)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		$woonplaats="";
		$straat="";
		$postcode='';
		$data=array();
		$httpfile='';
		$aParms=$request->getParsedBody();
		if(isset($aParms))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": zoekpostcode parsbody aParms:".urldecode(http_build_query($aParms,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
			if (isset($aParms["postcode"]))$postcode=strtoupper(trim($aParms['postcode']));else $postcode='';
			$postcode=str_replace(' ','',$postcode);
			if (isset($aParms['nr']))$huisnr=rtrim($aParms['nr']);else $huisnr="";
			try {
				$httpfile  = file_get_contents("https://www.postcode.nl/".substr($postcode,0,6).'/'.$huisnr);  // https://www.postcode.nl/zoek/2716lr%2F116
			} catch(Exception $e) {
				$httpfile='';
			} 
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek postcode httpfile gelezen: '.strval($httpfile)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
		}		
		if ($httpfile)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek postcode httpfile: '.$httpfile."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
			$pos1=strpos($httpfile,'<dt>Straatnaam</dt>');
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek postcode pos1 straatnaam: '.$pos1."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
			if ($pos1>0)$pos1=strpos($httpfile,'>',$pos1+20)+1;
			if ($pos1>0)
			{
				$posstr=strpos($httpfile,'<dl');
				$posstr=strpos($httpfile,'>',$posstr+2);
				$pclen=strpos($httpfile,'</dl>')-$posstr;
				$pcstring=substr($httpfile,$pos1,$pclen);
	//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek postcode pcstring: '.$pcstring."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
	
				$pos1=strpos($pcstring,'</dd>');
				if($pos1>1)
				{
					$straat=trim(substr($pcstring,0,$pos1));
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek postcode : straat:'.strtolower($straat)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
					$pos1=strpos($pcstring,"<dt>Woonplaats</dt>",$pos1+4);
					if ($pos1>1)
					{
						$pcstring=substr($pcstring,$pos1);
						$pos1=strpos($pcstring,'>',22)+1;
						$pos2=strpos($pcstring,'</dd>',$pos1);
						$woonplaats=html_entity_decode(trim(substr($pcstring,$pos1,$pos2-$pos1)));
						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek postcode : woonplaats:'.strtolower($woonplaats)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
						if (strpos(strtolower($woonplaats),'gravenhage')!==false)$woonplaats='Den Haag';
						$woonplaats=ucwords(strtolower($woonplaats));				
					}
				}
			}
		}

		$data= array('status'=>'success','message'=>'','woonplaats' => $woonplaats,'straat'=> $straat);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'json getting role:'.json_encode($data,JSON_HEX_TAG)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.log');
		$response->getBody()->write(json_encode($data));
		return $response;
	}
}


