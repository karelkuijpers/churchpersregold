<?php
namespace Parousia\Churchpersreg\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;
//use TYPO3\CMS\Fluid\View\TemplateView;
use TYPO3\CMS\Fluid\View\StandaloneView;
/*use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContextFactory;
use TYPO3\CMS\Extbase\Mvc\Web\Request as WebRequest;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext; */

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

 Class FluidTemplate
 {
 
 	/**
	 * Returns the rendered fluid email template
	 * @param string $template
	 * @param array $assign
	 * @param string $ressourcePath
	 * @return string
	 */
	static function render($template, Array $assign = array(), $caller,$extensionname) {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluid render start template:'.$template."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
//		$view=GeneralUtility::makeInstance(TemplateView::class);
		$view = GeneralUtility::makeInstance(StandaloneView::class);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate view created'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		$rootPath='EXT:'.$extensionname.'/Resources/Private/';
	    $ressourcesPath = GeneralUtility::getFileAbsFileName($rootPath);
//		$view->setTemplateRootPaths(array($ressourcesPath.'Templates'));
		$layoutPath=$ressourcesPath . 'Layouts/';
		$layoutPaths=array($layoutPath);
	    $view->setLayoutRootPaths($layoutPaths);
//		$view->setTemplate($template);
		$templatePath=$ressourcesPath . 'Templates/'.$template;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate templatePath : '.$templatePath."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
	    $view->setTemplatePathAndFilename($templatePath);
/*		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate after setTemplatePathAndFilename'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log'); */
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate after setLayoutRootPaths' ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');

/*		$objectManager=GeneralUtility::makeInstance(ObjectManager::class);
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluid objectManager instantiated'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
        //$context = $objectManager->get(RenderingContext::class, $caller);
		$context = (new RenderingContextFactory())->create();
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluid context instantiated'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
        $request = $objectManager->get(WebRequest::class);
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluid request class'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
        $request->setRequestUri(GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        $request->setBaseUri(GeneralUtility::getIndpEnv('TYPO3_SITE_URL'));
		$request->setControllerExtensionName($extensionname);
        $controllerContext = $objectManager->get(ControllerContext::class);
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluid controllerContext'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
        $controllerContext->setRequest($request);
        $context->setControllerContext($controllerContext);
 		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluid make view'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		
		$view->setRenderingContext($context); */
		//$assign['lll']='LLL:EXT:page_speed/Resources/Private/Language/locallang.xlf:';
	    $view->assignMultiple($assign);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'renderFluidTemplate assign'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
		return $view->render();
	}
}