<?php
namespace Parousia\Churchpersreg\Hooks;

ini_set("display_errors",1);
ini_set("log_errors",1);
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;

class changepersbyowner 
{
	protected $db;

/**
 * @param ServerRequestInterface $request
 * @param ResponseInterface $response
 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		/*
		* file for ajax to update person mobieltelnr by owner
		 * Created on 1 dec 2021
		 * Post parameters: 
		 	idperson,
			mobieltelnr,
		 */
	    
		//$person_id=69;
		session_start();
		$response = GeneralUtility::makeInstance(Response::class);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin changepersbyowner: "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');

		$aParms=$request->getParsedBody(); 
	   //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin changepersbyowner aParms:".http_build_query($aParms,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
		//if (!empty($_SESSION["permissie"]))
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'changepersbyowner session permissie : '.http_build_query($_SESSION,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
	    //error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": begin changepersbyowner permissie_array_:".http_build_query($permissie_array_,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');

		//$userid = $chuser['person_id'];

		if (isset($aParms["idperson"]))$person_id=$aParms["idperson"];
		else die("You are not privileged to perform this action");
		if (!empty($_SESSION["permissie"])){
			$userid = $_SESSION['userid'];
			if ($userid!=$person_id)
			{
				$response->getBody()->write("You are not privileged to perform this action");
				return $response;
			}
		}
		else 
		{
			$response->getBody()->write("Sessie verlopen. Ververs het scherm.");
			return $response;
		}

//		get current values:
		churchpersreg_div::connectdb($this->db);
		// get old values:
		$query='select AES_DECRYPT(mobieltelnr,@password) as mobieltelnrold,AES_DECRYPT(emailadres,@password) as emailadresold from persoon where uid="'.$person_id.'"';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": mobieltelnrold select person:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
		$result=$this->db->query($query) or die("Can't perform Query");	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'mobieltelnrold error: '.$this->db->error."; query:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
		$rowold=$result->fetch_array(MYSQLI_ASSOC);
		$mobieltelnrold=$rowold['mobieltelnrold'];
		$emailadresold=$rowold['emailadresold'];
		$mobieltelnr='';
		$emailadres='';
		if (isset($aParms["mobieltelnr"]))$mobieltelnr=$aParms["mobieltelnr"];
		if (isset($aParms["emailadres"]))$emailadres=$aParms["emailadres"];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'changepersbyowner $emailadres: '.$emailadres."; emailadresold:".$emailadresold."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');		
		if (!empty($mobieltelnrold) && (empty(intval($mobieltelnr)) || empty(intval(substr($mobieltelnr,-8)))) )
		{
			$response->getBody()->write("Formaat mobielnummer is onjuist");
			return $response;
		}
		
		if (!empty($emailadresold) && empty($emailadres) )
		{
			$response->getBody()->write("Formaat emailadres is onjuist");
			return $response;
		}
		$statement='';
		$valueschanged=array();
		// bij fout met lege id en foutmelding in self::$ErrMsg)
		if ($mobieltelnrold !== $mobieltelnr)
		{
			$regex = '/^(\+31|0)6-?[\d]{8}$/';
			if (!preg_match($regex, $mobieltelnr)) 
			{
				$response->getBody()->write("Formaat mobielnummer ".$mobieltelnr." is onjuist");
				return $response;
			}
			$valueschanged["mobieltelnr"]=array("old"=>$mobieltelnrold,"new"=>$mobieltelnr);
			$statement='mobieltelnr=AES_ENCRYPT("'.$mobieltelnr.'",@password)';
		}
		if ($emailadresold!==$emailadres)
		{
			$regex = '/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/';
			if (!preg_match($regex, $emailadres)) 
			{
				$response->getBody()->write("Formaat emailadres is onjuist");
				return $response;
			}
			$valueschanged["emailadres"]=array("old"=>$emailadresold,"new"=>$emailadres);
			$statement.=(empty($statement)?'':',').'emailadres=AES_ENCRYPT("'.$emailadres.'",@password)';
		}
		$statement='update persoon set '.$statement.' where uid="'.$person_id.'"';
		$changedjson=json_encode($valueschanged);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'changepersbyowner changedjson: '.$changedjson."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
		
		//$statement='update persoon set mobieltelnr=AES_ENCRYPT("'.$mobieltelnr.'",@password) where uid="'.$person_id.'"';
		$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'changepersbyowner error: '.$this->db->error."; statement:".$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
		if ($this->db->affected_rows >0)
		{
			//something changed:
			$statement='update persoon set steller="'.$person_id.'",datum_wijziging=datum_wijziging=NOW() where uid="'.$person_id.'"';
			$results=$this->db->query($statement);
			// add to log:
			$rownew=array();
			$rownew['mobieltelnr']=$mobieltelnr;
//			$statement='insert into `updatelog` (id_author,tablename,uid_table,oldvalue) values ('.$person_id.',"updatelog",'.$person_id.',\''.json_encode($rowold)."','".json_encode($rownew)."')";
			$statement='insert into `updatelog` (id_author,tablename,uid_table,changedvalue) values ('.$person_id.',"persoon",'.$person_id.',\''.$changedjson."')";
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'changepersbyowner logging statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
			$results=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'changepersbyowner logging error: '.$this->db->error."; statement:".$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/changepersbyowner.log');
		}

		$response->getBody()->write('');
		return $response;
	}
	 
}

 

