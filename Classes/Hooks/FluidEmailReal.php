<?php
namespace Parousia\Churchpersreg\Hooks;

use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;


//***
//	This class makes Mailmessage to send always driectly e-mials even when transport_spool_type=file
//
//**

class FluidEmailReal extends FluidEmail
{
    /**
     * @var Mailer
     */
    protected $mailer;

	/**
     * Sends the message.
     *
     * This is a short-hand method. It is however more useful to create
     * a Mailer instance which can be used via Mailer->send($message);
     *
     * @return bool whether the message was accepted or not
     */
    public function send(): bool
    {
        $this->mailer = GeneralUtility::makeInstance(Mailer::class);
    	$transport=$this->mailer->getRealTransport();
		$this->mailer = new Mailer($transport);
  		//$this->initializeMailer();
        $this->sent = false;
        $this->mailer->send($this);
        $sentMessage = $this->mailer->getSentMessage();
        if ($sentMessage) {
            $this->sent = true;
        }
        return $this->sent;
    }	
}