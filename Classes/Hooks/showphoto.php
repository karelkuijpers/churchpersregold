<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2007 k.kuijpers <karelkuijpers@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/* Shows photo from person with Pid
*/
//require_once('../inc/globalpar.php');

$Pid='';
if (isset($_GET["Pid"]))$Pid=trim($_GET["Pid"]);
if (!is_numeric($Pid))exit();
$size=$_GET["size"];
$localConf = include $_SERVER['DOCUMENT_ROOT'].'/typo3conf/LocalConfiguration.php';
$db = new mysqli($localConf['DB']['Connections']['Default']['host'], $localConf['DB']['Connections']['Default']['user'], $localConf['DB']['Connections']['Default']['password'], $localConf['DB']['Connections']['Default']['dbname']) or die("Unable to connect to SQL server");;
$result=$db->query("SELECT foto,mimetypefoto,photoWidth,photoHeight,thumbnail FROM persoon WHERE uid='".$Pid."'");
$obj=$result->fetch_object();
ob_start();
header("Content-Type: ".$obj->mimetypefoto);
if ($size=='small' && !empty($obj->thumbnail)) 
	echo $obj->thumbnail;
else 
	echo $obj->foto;
ob_end_flush(); 
?>
