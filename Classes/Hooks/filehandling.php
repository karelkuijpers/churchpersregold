<?php
namespace Parousia\Churchpersreg\Hooks;

ini_set("display_errors",1);
ini_set("log_errors",1);
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;

/*
* class for handling uploads of files
*/

class filehandling 
{

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
	 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{

		$response = GeneralUtility::makeInstance(Response::class);
		$aParms=$request->getParsedBody(); 

		if (isset($aParms["action"]))$action = $aParms["action"]; else $action='';			
		$content='';
		if ($action=='laden')
		{
			if(empty($_FILES))
				echo "selecteer eerst een bestand";
			else
			{
				$filename=$_FILES['photos']['tmp_name'][0];
				$mimetype=$_FILES['photos']['type'][0];
				$size=$_FILES['photos']['size'][0];
				$person_id = $aParms["idperson"];
				$error=$_FILES['photos']['error'][0];
				$max=$aParms["MAX_FILE_SIZE"];
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": laden foto: idperson=$person_id; filename=$filename; mimetype= $mimetype; size=$size max:$max"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/filehandler.log');
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'laden: '.http_build_query($_FILES,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/filehandler.log');
		
		
				if (!empty($filename))
				{	
					$tmpImg="";
					$thumbnail="";
					$newwidth=400;
					$newheight=533;
					$error="";
					if (churchpersreg_div::resizeImage($filename,$newwidth,$newheight,$tmpImg,$thumbnail,$error))
					{	
						// update person:
						churchpersreg_div::connectdb($db);
						$statement='update persoon set foto="'.$db->real_escape_string($tmpImg).'",mimetypefoto="image/jpeg",photoWidth="'.$newwidth.'",photoHeight="'.$newheight.'",thumbnail="'.$db->real_escape_string($thumbnail).'" where uid="'.$person_id.'"';
						$db->query($statement);
						$sqlerr=$db->error;
						if (!empty($sqlerr))   
						{	
							//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'laden update : '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/filehandler.log');
							$content="laden foto mislukt";
						}
					}
					else $content.=$error;
				}
				else $content.="Niets geladen; foto groter dan $max bytes?<p>";
				$response->getBody()->write($content);
				return $response;
			}
		}
		elseif($action=='verwijderen')
		{
			$person_id = $_POST["idperson"];
			if (!empty($person_id))
			{
									// update person:
				churchpersreg_div::connectdb($db);
				$statement='update persoon set foto="",mimetypefoto="",thumbnail="" where uid="'.$person_id.'"';
				$db->query($statement);
				$response->getBody()->write($content);
				return $response;
			}
		}
	}
}
?>