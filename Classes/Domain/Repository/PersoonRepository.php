<?php

namespace Parousia\Churchpersreg\Domain\Repository;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Configuration\ConfigurationManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

ini_set("display_errors",1);
ini_set("log_errors",1);

/**
 * Class PersoonRepository
 *
 * @package Parousia\Churchpersreg\Domain\Repository
 *
 * return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class PersoonRepository extends AbstractRepository
{
	var $ErrMsg=""; //error message
	var $personReg=false;
	var $cleanteamReg=false;
	var $userid="";
	var $query;
	var $db;
	protected $aExport;

	
	public function findSelection(array $search = null)
	{
		$this->initializeObject();
		$query = $this->createQuery();

		if (empty($search['peradres'])) $statement=$this->GetPersons($search); 
		else $statement=$this->GetAddresses($search); 
	//	var_dump($statement);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelection('.$search['per_adres'].'): '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		$query->statement($statement);
		
 		try {
		      $result= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $result;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }

	public function findExportPersons(array $search = null)
	{
		$this->initializeObject();
		$query = $this->createQuery();

		$statement=$this->GetExportPersons($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelection: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		$query->statement($statement);
		
 		try {
		      $result= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $result;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }

	 /**
     * Override default findByUid function 
     *
     * param int  $uid                 id of record
     *
     * return persoon
     */
    public function findByUid($uid)
	{
		$persoon=array();
		if (empty($uid))return $persoon;
        $statement=$this->getPerson($uid);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

//		var_dump($statement);
//		$query->statement($statement);
//        $results= $query->execute(true);
		churchpersreg_div::connectdb($this->db);
		$result=$this->db->query($statement) or die("Can't perform Query");	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getstatusaccount query:"'.$query.'"; error:'.$this->db->error.'; aantal:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
		If (empty($this->db->error) and $result->num_rows == 1)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getstatusaccount num_rows: '.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
			$persoon=$result->fetch_array(MYSQLI_ASSOC);

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid persoon: '.urldecode(http_build_query($persoon,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');	

			if (!empty($persoon['huisgenoten']))$huisgenoten=explode(', ',$persoon['huisgenoten']);else $huisgenoten=array();
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'huisgenoten: '.$persoon['huisgenoten']."; aantal:".count($huisgenoten)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			if (count($huisgenoten)<2) 
			{	$persoon['huisgenoten']='';  // only one person at address
				$persoon['moveall']="1";
			}
			$statement=$this->getChilds($uid);
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid childs statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			//$query->statement($statement);
	        //$children= $query->execute(true);
			$result=$this->db->query($statement) or die("Can't perform Query");	
			$children=$result->fetch_array(MYSQLI_ASSOC);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'PersoonRepository findByUid children: '.urldecode(http_build_query($children,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');	


	//		var_dump($results[0]);
			$persoon['children']=$children['childs'];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'children: '.$persoon['children']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		}
//		var_dump($persoon);
		return $persoon;
	}

	 /**
     * Override default findRecipients function 
     *
     * param array Recipients            // arays with {emailaddress<full name>}
     *
     * return persons
     */
    public function findRecipients(string $Recipients)
	{
		$this->initializeObject();
		$query = $this->createQuery();
        $statement='select uid,AES_DECRYPT(emailadres,@password) as emailadres,roepnaam,trim(replace(concat(roepnaam,if(tussenvoegsel="",""," "),tussenvoegsel," ",achternaam),"  "," ")) as naam, geslacht'.
		', trim(replace(concat(tussenvoegsel,if(tussenvoegsel="",""," "),achternaam),"  "," ")) as lastname'.
		' from persoon'.
		' where deleted=0 and find_in_set(concat(AES_DECRYPT(emailadres,@password),"<",trim(replace(concat(roepnaam,if(tussenvoegsel="",""," "),tussenvoegsel," ",achternaam),"  "," ")),">"),"'.$Recipients.'")';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findRecipients statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		$query->statement($statement);
        $persons= $query->execute(true);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findRecipients persons: '.urldecode(http_build_query($persons,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		return $persons;
	}

	
	 /**
     * Override default findSelectionAanvraag function 
     *
     * param int  $uid                 id of record
     *
     * return persoon
     */
    public function findSelectionAanvraag(array $search)
	{
		$this->initializeObject();
		$query = $this->createQuery();
		//if (!empty($uid)) $search.="uidpersoon=".(intval($uid))." ";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionAanvraag search : '.urldecode(http_build_query($search,NULL,"=")).'; typerequest: '.$search['typerequest']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$zoekargument=$this->ComposeSearch($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons:bezoeker: '.$Bezoeker."; zoekargument:".$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$zoekargument.=" and a.uidpersoon=tp.uid";
		
		if (!empty(trim($search['typerequest'])))
			$zoekargument.=" and typerequest='".$search['typerequest']."'";
		if (!empty($search['uidpersoon']))
			$zoekargument.=" and a.uidpersoon='".intval($search['uidpersoon'])."'";
		if (!empty($search['openonly']))
			$zoekargument.=" and (case a.typerequest when 'doop' then tp.doopdatum when 'lidmaatschap' then tp.datum_lidmaatschap end)='0000-00-00'";
		
		$fields='a.uidpersoon,a.typerequest, a.datumaanvraag,concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) as naam,concat(tpartner.roepnaam," ",tpartner.tussenvoegsel," ",'.
		'tpartner.achternaam) as partnernaam,tpartner.uid as uidpartner,case a.typerequest when "doop" then tp.doopdatum when "lidmaatschap" then tp.datum_lidmaatschap end as ingevoerd,a.id_bezoeker';
		$selectaanvraag="select ".$fields.' from (aanvraag a left join persoon as tpartner on a.uidpartner=tpartner.uid),'.
		'persoon tp ,adres as ta'.
		' where '.$zoekargument;

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionAanvraag: selectaanvraag: '.$selectaanvraag."; zoekargument:".$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

//		var_dump($statement);
/*		$query->statement($selectaanvraag);
        $aanvragen= $query->execute(true);
		
		// add aansluiting aanvragen:  
		$statement='select 0 as uidpersoon, "aansluiting" as typerequest, from_unitime(`crdate`) as datumaanvraag, concat(replace(result->\'$."text-1"\',\'"\',\'\')," ",replace(result->\'$."text-2"\',\'"\',\'\')) as naam '.
		'from tx_formtodatabase_domain_model_formresult '.
		'left join (select p.uid,p,achternaam from persoon as p,on('; */
		
		$zoekargument=$this->ComposeSearchRequests($search['general']);
		$where='';
		if (!empty($search['openonly']))
			$where.=" where p.invoerdatum IS NULL ";
		if (!empty($search['typerequest']) )
		{
			if (!empty($where))$where.=" and ";else $where=" where ";
			$where.="f.typerequest='".$search['typerequest']."'";
		}
		
		$selectform= 'select  f.uidpersoon, f.typerequest, f.datumaanvraag, f.naam, f.partnernaam,f.uidpartner, p.invoerdatum,f.id_bezoeker '. 
		' from (select uid as uidpersoon,"aansluiting" as typerequest, from_unixtime(`crdate`) as datumaanvraag, concat(replace(result->\'$."text-1"\',\'"\',\'\')," ",replace(result->\'$."text-2"\',\'"\',\'\')) as naam," " as partnernaam,"" as uidpartner,id_bezoeker, '.
		' STR_TO_DATE(replace(result->\'$."text-6"\',\'"\',\'\'),"%d-%m-%Y") as geboortedatum, replace(result->\'$."text-4"\',\'"\',\'\') as postcode,replace(result->\'$."text-7"\',\'"\',\'\') as huisnummer from tx_formtodatabase_domain_model_formresult where '.$zoekargument.') as f'.
		' left join (select p.uid,p.invoerdatum, p.geboortedatum, a.postcode, a.huisnummer from persoon p right join adres a on (AES_DECRYPT(p.id_adres,@password)=a.uid) where  p.deleted=0 and a.deleted=0) as p '.
		' on (f.geboortedatum=p.geboortedatum and f.postcode=p.postcode and f.huisnummer=p.huisnummer)'.$where;

//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionAanvraag: aansluitingen selectform: '.$selectform."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
/*		$query->statement($selectform);
        $aansluitingen= $query->execute(true); 
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionAanvraag aansluitingen : '.urldecode(http_build_query($aansluitingen,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
*/



		$statementunion="(".$selectaanvraag.") union (".$selectform.")".' order by datumaanvraag desc';
		$statement='select u.*,group_concat(b.roepnaam separator ", ") as bezoekers from ('.$statementunion.') as u left join persoon b on (find_in_set(b.uid,u.id_bezoeker) and b.deleted=0  ) group by u.uidpersoon,u.typerequest order by u.datumaanvraag desc';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionAanvraag: union statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$query->statement($statement);

        $aanvragen= $query->execute(true);
		return $aanvragen;
	}

	 /**
     * Override default findAanvraagByUid function 
     *
     * param int  $uid                 id of record
     *
     * return aanvraag
     */
    public function findAanvraagByUid($uid)
	{
		$this->initializeObject();
		$query = $this->createQuery();
		
		$statement = 'select f.*,group_concat(concat(s.roepnaam,if(s.tussenvoegsel="",""," "),s.tussenvoegsel," ",s.achternaam) separator ",") as bezoekers,p.invoerdatum '.
		' from (select uid,"aansluiting" as typerequest, from_unixtime(`crdate`) as datumaanvraag, concat(replace(result->\'$."text-1"\',\'"\',\'\')," ",replace(result->\'$."text-2"\',\'"\',\'\')) as naam," " as partnernaam,"" as uidpartner,id_bezoeker, '.
		' STR_TO_DATE(replace(result->\'$."text-6"\',\'"\',\'\'),"%d-%m-%Y") as geboortedatum, replace(result->\'$."text-4"\',\'"\',\'\') as postcode,replace(result->\'$."text-7"\',\'"\',\'\') as huisnummer from tx_formtodatabase_domain_model_formresult where uid='.$uid.') as f'.
		' left join (select p.uid,p.invoerdatum, p.geboortedatum, a.postcode, a.huisnummer from persoon p right join adres a on (AES_DECRYPT(p.id_adres,@password)=a.uid) where  p.deleted=0 and a.deleted=0) as p '.
		' on (f.geboortedatum=p.geboortedatum and f.postcode=p.postcode and f.huisnummer=p.huisnummer)'.
		" left join persoon s on(find_in_set(s.uid,f.id_bezoeker) and s.deleted=0)";
		
		$statement = 'select f.*,group_concat(concat(s.roepnaam,if(s.tussenvoegsel="",""," "),s.tussenvoegsel," ",s.achternaam) separator ",") as bezoekers,p.invoerdatum '.
		' from (select *, '.
		' STR_TO_DATE(replace(result->\'$."text-6"\',\'"\',\'\'),"%d-%m-%Y") as geboortedatum, replace(result->\'$."text-4"\',\'"\',\'\') as postcode,replace(result->\'$."text-7"\',\'"\',\'\') as huisnummer from tx_formtodatabase_domain_model_formresult where uid='.$uid.') as f'.
		' left join (select p.uid,p.invoerdatum, p.geboortedatum, a.postcode, a.huisnummer from persoon p right join adres a on (AES_DECRYPT(p.id_adres,@password)=a.uid) where  p.deleted=0 and a.deleted=0) as p '.
		' on (f.geboortedatum=p.geboortedatum and f.postcode=p.postcode and f.huisnummer=p.huisnummer)'.
		" left join persoon s on(find_in_set(s.uid,f.id_bezoeker) and s.deleted=0)";


		$query->statement($statement);		

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findAanvraagByUid: statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

        $requests= $query->execute(true);
		//var_dump($requests[0]);
		$request=$requests[0];
		$results=json_decode($request['result'],true,20,JSON_INVALID_UTF8_IGNORE);
		$aanvraag=$request + $results;
		
		return $aanvraag;


	}

	 /**
     * Override default findRequestByUid function 
     *
     * param int  $uid                 id of record
     *
     * return aanvraag
     */
    public function findRequestByUid($uid,$typerequest)
	{
		$this->initializeObject();
		$query = $this->createQuery();
		$statement='SELECT f.*,group_concat(concat(s.roepnaam,if(s.tussenvoegsel="",""," "),s.tussenvoegsel," ",s.achternaam) separator ",") as bezoekers,case f.typerequest when "doop" then tp.doopdatum when "lidmaatschap" then tp.datum_lidmaatschap end as invoerdatum'.
		' FROM (`aanvraag`as f'.
		' left join persoon as s on(find_in_set(s.uid,f.id_bezoeker) and s.deleted=0)), persoon as tp'.
		" where tp.uid=f.uidpersoon and tp.deleted=0 and f.uidpersoon=".$uid." and f.typerequest='".$typerequest."'";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findRequestByUid: statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$query->statement($statement);		


        $requests= $query->execute(true);
//		var_dump($results[0]);
		$aanvraag=$requests[0];
		
		return $aanvraag;

	}


	/**
	 * Method 'SaveAanvraag' for the 'churchregpers' extension.
 	*
	 * param person required 
	 * returns true or error message
	 */
	
	public function SaveAanvraag($uid,$id_bezoeker,$typerequest)
	{
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SaveAanvraag id_bezoeker: '.$id_bezoeker."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->ErrMsg="";
		if (! churchpersreg_div::Heeftpermissie("Persoon Schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		if ($typerequest=='aansluiting')
			$statement='update `tx_formtodatabase_domain_model_formresult` set `id_bezoeker`="'.$id_bezoeker.'" where uid="'.$uid.'"';
		else
			$statement='update `aanvraag` set `id_bezoeker`="'.$id_bezoeker.'" where uidpersoon="'.$uid.'"';
		
		$results=$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": SaveAanvraag query: ".$statement."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		return $this->ErrMsg;
	}


	/**
	 * Method 'DeleteAanvraag' for the 'churchregpers' extension.
 	*
	 * param person required 
	 * returns true or error message
	 */
	
	public function DeleteAanvraag($uid,$typerequest)
	{
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'DeleteAanvraag uid: '.$uid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->ErrMsg="";
		if (! churchpersreg_div::Heeftpermissie("Persoon Schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		if ($typerequest=='aansluiting')
			$statement='update `tx_formtodatabase_domain_model_formresult` set `deleted`="1" where uid="'.$uid.'" and deleted="0"';
		else
			$statement='update `aanvraag` set `deleted`="1" where uidpersoon="'.$uid.'" and deleted="0"';
		
		$results=$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": DeleteAanvraag query: ".$statement."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		return $this->ErrMsg;
	}

	 /**
     * Override default findSelectionUpdatelog function 
     *
     * param array search                 selection arguments
     *
     * return updatelog
     */
    public function findSelectionUpdatelog(array $search)
	{
		$this->initializeObject();
		$query = $this->createQuery();
		//if (!empty($uid)) $search.="uidpersoon=".(intval($uid))." ";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionUpdatelog search : '.urldecode(http_build_query($search,NULL,"=")).'; typerequest: '.$search['typerequest']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$zoekargument=$this->ComposeSearch($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons:bezoeker: '.$Bezoeker."; zoekargument:".$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$zoekargument.=" and a.uid_table=tp.uid";
		
		if (isset($search['tablename']) and !empty(trim($search['tablename'])))
			$zoekargument.=" and a.tablename='".$search['tablename']."'";
		if (isset($search['uid_table']))
			$zoekargument.=" and a.uid_table='".intval($search['uid_table'])."'";
		if (isset($search['emptied']))
			$zoekargument.=" and changedvalue like '%\"new\":\"\"%' "; 
		
		$fields='a.uid,a.uid_table,a.`tablename`, a.date_change,concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) as naam,a.tablename,a.id_author,a.changedvalue';
		$selectUpdatelog="select ".$fields." from updatelog a, persoon tp ".
		' left join persoon as tpartner on tp.id_partner=tpartner.uid,adres as ta'.
		' where '.$zoekargument.' order by date_change desc';

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionUpdatelog: selectUpdatelog: '.$selectUpdatelog."; zoekargument:".$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		
		$query->statement($selectUpdatelog);

        $updatelogs= $query->execute(true);
		foreach ($updatelogs as $key=>$updatelog)
		{
			$changedvalues=json_decode($updatelog['changedvalue'],true);
			//$chgstr=GeneralUtility::array2xml($changedvalues);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelectionUpdatelog: selectUpdatelog changed values: '.$chgstr."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			$chgstr=implode(', ',array_keys($changedvalues));
			$updatelogs[$key]['changestr']=$chgstr;

		}
		return $updatelogs;
	}

	 /**
     * Override default findUpdatelogByUid function 
     *
     * param int  $uid                 id of record
     *
     * return Updatelog
     */
    public function findUpdatelogByUid($uid)
	{
		$this->initializeObject();
		$query = $this->createQuery();
		$statement="SELECT a.*,concat(s.roepnaam,if(s.tussenvoegsel='','',' '),s.tussenvoegsel,' ',s.achternaam) as naam, s.geboortedatum ".
		"FROM `updatelog` as a, persoon s ".
		" where a.uid=".$uid. " and s.uid=a.uid_table and s.deleted=0";
		$query->statement($statement);		

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findUpdatelogByUid: statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

        $logs= $query->execute(true);
//		var_dump($results[0]);
		$log=$logs[0];
		$log['changedvalues']=json_decode($log['changedvalue'],true,20,JSON_INVALID_UTF8_IGNORE);
		$Updatelog=$log;
		
		return $Updatelog;

	}


	
	
	/**
	 * Method 'ComposeSearchRequests' for the 'churchpersreg' extension.
 	*
	 * param searchfield with keywords to search 
	 * returns composed sql search parameter
	 */
	function ComposeSearchRequests($search = null)
	{	
	//	Compose zoekargument for searching within verzoekomaansluiting.form

		$zoekargument="";
		$Not=false;
		$a_key=$this->DeriveKeywords($search);
		foreach ($a_key as $key)
		{	$key=trim($key);
			if (!empty($key))
			{	// key:
				if (substr($key,0,1)=="-")
				{	
					$zoekargument.="NOT ";  // ontkenning
					$key=substr($key,1);
					$Not=true;
				}			
				$key=addslashes($key);
					//TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekwoord:".$key,"churchpersreg");
				$zoekargument.=' (concat(replace(result->\'$."text-1"\',\'"\',\'\')," ",replace(result->\'$."text-2"\',\'"\',\'\')) like "%'.$key.'%" OR ';
				// ook op andere velden zoeken:
				$zoekargument .=  'replace(result->\'$."text-1"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-2"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-4"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-7"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-3"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-5"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-8"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-9"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."telephone-1"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."telephone-2"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."email-1"\',\'"\',\'\') like "%'.$key.'%" OR ';
				$zoekargument .=  'replace(result->\'$."text-5"\',\'"\',\'\') like "%'.$key.'%") and ';
			}
		}
		$zoekargument.= " deleted=0";
		return $zoekargument;
	}

	
	 /**
     * Override default findArchives function 
     *
     * param int  $personid                 id of person
     *
     * return archives
     */
    public function findArchives($personid,$userid)
	{
	//	$this->initializeObject();
	//	$query = $this->createQuery();
		$confidential= (int)churchpersreg_div::Heeftpermissie("ContactArchief lezen vertrouwelijk");	
		//$userid=$GLOBALS['TSFE']->fe_user->user['person_id'];
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": findArchives permissie lezen: ".(int)churchpersreg_div::Heeftpermissie("ContactArchief lezen")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": get archive lezen: ".(church_div::Heeftpermissie("ContactArchief lezen")?"ja":"nee").", confid: ".(church_div::Heeftpermissie("ContactArchief lezen vertrouwelijk")?"ja":"nee").", schrijven: ".(church_div::Heeftpermissie("ContactArchief schrijven")?"ja":"nee")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/pi3/getarchive.log');
/*		if (!$confidential and (int)churchpersreg_div::Heeftpermissie("ContactArchief lezen"))
		{ 
			$alleenschrijven = " and id_person_owner= '".$userid."'"; 
			$confidential=true;
		}
		else $alleenschrijven = ""; */
		$condition="";
		if (!$confidential)
		{
			if (!(int)churchpersreg_div::Heeftpermissie("ContactArchief lezen")) $condition.=" and id_person_owner= '".$userid."' ";
			else $condition.=" and (!confidential or id_person_owner= '".$userid."' )";
		}
		$statement='select uid, id_parentarchive,id_person,AES_DECRYPT(subject,@password) as subject,author,id_person_owner,date,AES_DECRYPT(document_name,@password) as document_name,'.
		'type_content,AES_DECRYPT(content,@password) as content,confidential from contact_archive '.
		" where id_person='".$personid."' ".$condition." order by date desc,uid";
//		" where id_person='".$personid."'".(($confidential)?"":" and !confidential").$alleenschrijven." order by date desc,uid";
	//	$query->statement($statement);
     //   $archives= $query->execute(true);
	 	churchpersreg_div::connectdb($this->db);
		$result=$this->db->query($statement) or die("Can't perform Query");	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'PersoonRepository findArchives statement:"'.$statement.'"; error:'.$this->db->error.'; aantal:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		If (empty($this->db->error) and $result->num_rows == 1)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getstatusaccount num_rows: '.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchlogin/Classes/Controller/debug.txt');
			$archives=$result->fetch_all(MYSQLI_ASSOC);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'PersoonRepository findArchives archives aantal: '.count($archives)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		}
		else $archives=array();

	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": get archive query: ".$statement."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		//$aantalgevonden=$archives>num_rows;
		//TYPO3\CMS\Core\Utility\GeneralUtility::devLog("gevonden in archief voor $pid : $aantalgevonden","churchpersreg");
		
		return $archives;		
	}


	 /**
     * Override default findArchive function 
     *
     * param int  $archiveid                 id of archive
     *
     * return archive
     */
    public function findArchive($id_archive,$userid)
	{
		$this->initializeObject();
		$query = $this->createQuery();
		$confidential= (int)churchpersreg_div::Heeftpermissie("ContactArchief lezen vertrouwelijk");	
		$userid=$GLOBALS['TSFE']->fe_user->user['person_id'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": get archive lezen: ".(church_div::Heeftpermissie("ContactArchief lezen")?"ja":"nee").", confid: ".(church_div::Heeftpermissie("ContactArchief lezen vertrouwelijk")?"ja":"nee").", schrijven: ".(church_div::Heeftpermissie("ContactArchief schrijven")?"ja":"nee")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/pi3/getarchive.log');
		$condition="";
		if (!$confidential)
		{
			if (!(int)churchpersreg_div::Heeftpermissie("ContactArchief lezen")) $condition.=" and id_person_owner= '".$userid."' ";
			else $condition.=" and (!confidential or id_person_owner= '".$userid."' )";
		}
/*		if (!$confidential and (int)churchpersreg_div::Heeftpermissie("ContactArchief lezen"))
		{ 
			$alleenschrijven = " and id_person_owner= '".$userid."'"; 
			$confidential=true;
		} */
		else $alleenschrijven = "";
		$statement='select uid, id_parentarchive,id_person,AES_DECRYPT(subject,@password) as subject,author,id_person_owner,date,AES_DECRYPT(document_name,@password) as document_name,'.
		'type_content,AES_DECRYPT(content,@password) as content,confidential from contact_archive '.
		" where uid='".$id_archive."' ".$condition;
//		" where uid='".$id_archive."'".(($confidential)?"":" and !confidential").$alleenschrijven;
		$query->statement($statement);
        $archives= $query->execute(true);
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findArchive error:'.$this->db->error.'; statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findArchive archives: '.urldecode(http_build_query($archives,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		//if ($archives->num_rows==0) return NULL;
		$archive=$archives[0];
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findArchive archive: '.urldecode(http_build_query($archive,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if ($archive['type_content']=='email')
		{
			// Get attachements:
//			$statement='select group_concat( AES_DECRYPT(document_name,@password) separator "; ") as attachements from contact_archive where id_parentarchive="'.$id_archive.'"'.(($confidential)?"":" and !confidential");
			$statement='select group_concat( AES_DECRYPT(document_name,@password) separator "; ") as attachements from contact_archive where id_parentarchive="'.$id_archive.'"';
			$query->statement($statement);
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findArchive attachements  statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
    	    $attachements= $query->execute(true);
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findArchive attachements error:'.$this->db->error.'; attachements: '.$attachements."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			$archive['attachements']=$attachements[0]['attachements'];
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findArchive archives met attachemnts: '.urldecode(http_build_query($archive,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		}
		return $archive;		
	}
	
	/**
	 * Method 'FindListPersons' for the 'churchregpers' extension.
 	*
	 * Get array with addresses to export
	 *
	 * param	string		$type: addressenlijst, ledenlijst
	 * returns	lines to be printed into array $this>aExport
	 */
	function FindListPersons($type = "ledenlijst")
	{	
		$this->initializeObject();
		$query = $this->createQuery();
		
		$where="where AES_DECRYPT(tp.id_adres,@password)=ta.uid and tp.deleted=0 and ta.deleted=0";
		if ($type=="ledenlijst"){$where.=" and lid='is lid'";}
		if ($type=="adressenlijst"){$where.=" and bezoeker='is bezoeker' ";}
		$statement='select tp.uid as id,achternaam, tussenvoegsel, voornamen, roepnaam'.
		', AES_DECRYPT(emailadres,@password) as emailadres'.
		', AES_DECRYPT(mobieltelnr,@password) as mobieltelnr'.
		', geslacht, burgerlijke_staat, id_partner'.
		', id_vader, id_moeder, email_op_website '.
		', straatnaam,huisnummer,postcode,woonplaats,land'.
		', AES_DECRYPT(telefoonnr_vast,@password) as telefoonnr_vast'.
		', tel_op_website,adres_op_website'.
		', AES_DECRYPT(tp.id_adres,@password) as id_adres'.
		' from persoon as tp, adres as ta '.$where;
		if ($type=="adressenlijst")$statement.= ' order by id_adres, burgerlijke_staat DESC, geslacht ASC';
		else $statement.= ' order by achternaam, roepnaam';
		$query->statement($statement);
      	$persons= $query->execute(true);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindListPersons persions:'.\TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($persons)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$curadr=0;
		$aGroep=array();
		$this->aExport=array();
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Getlist start aExport : '.is_array($this->aExport)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if ($type=="ledenlijst"){
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindListPersons type:'.$type.'; statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			foreach ($persons as $aPers)
			{
				$naam=$aPers["achternaam"].', ';
				if (!empty($aPers["tussenvoegsel"]))$naam.=$aPers["tussenvoegsel"].' ';
				$naam.=$aPers["roepnaam"];
				$telefoonnr=$aPers['mobieltelnr'];
				if (empty($telefoonnr)) $telefoonnr=$aPers['telefoonnr_vast'];
				$this->aExport[]=array($naam,$aPers["straatnaam"]." ".$aPers["huisnummer"],$aPers["postcode"],$aPers["woonplaats"],$aPers["land"],$telefoonnr,$aPers['emailadres']);
			}
			return $this->aExport;
		}		
		//for ($ptr=0;$ptr<$aantalgevonden;$ptr++)
		foreach ($persons as $curpers)
		{	//$curpers=$arrSelect[$ptr];
			if ($curadr<>$curpers["id_adres"])
			{	if ($curadr>0)
				{	// samenstellen adresregel:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindListPersons aGroep 1:'.\TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($aGroep)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
					$this->SamenstellenGroep($aGroep,$type);
					$aGroep=array();
				}
				$curadr=$curpers["id_adres"];
			}
			array_push($aGroep,$curpers);
		}
		// laatste entries ook uitvoeren:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindListPersons aGroep end:'.\TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($aGroep)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->SamenstellenGroep($aGroep,$type);
		// Sorteer export op naam:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindListPersons unsorted aExport:'.\TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($this->aExport)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		usort($this->aExport, array("Parousia\Churchpersreg\Domain\Repository\PersoonRepository","cmp")); 
		foreach ($this->aExport as &$row)   { unset($row[0]); }  //shift achternaam off
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'FindListPersons sorted aExport:'.\TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($this->aExport)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		return $this->aExport;
	}


	/**
	 * Method 'Save' for the 'churchregpers' extension.
 	*
	 * param person required 
	 * param userid required
	 * returns true or error message
	 */
	
	public function save(&$person,$userid)
	{
		$this->initializeObject();
		$this->query = $this->createQuery();
		$this->ErrMsg="";
		if (! churchpersreg_div::Heeftpermissie("Persoon Schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		$this->cleanteamReg = churchpersreg_div::Heeftpermissie("Cleanteams samenstellen");
		$person_id=intval($person['person_uid']);
//		$userid=$GLOBALS['TSFE']->fe_user->user['person_id'];

		$geboortedatum='';
		if (!empty($person['geboortedatum']))
		{
			$gbdat=new \DateTime($person['geboortedatum']);
			if (!is_object($gbdat))
			{
				$this->ErrMsg="geboortedatum ".$person['geboortedatum']." is een ongeldige datum";
				return $this->ErrMsg;
			}

			if ($gbdat > new \DateTime())
			{
				$this->ErrMsg="geboortedatum kan niet in de toekomst liggen";
				return $this->ErrMsg;
			}
			if ($gbdat < new \DateTime("- 130 years"))
			{
				$this->ErrMsg="geboortedatum - ".$person['geboortedatum']." - te ver in het verleden";
				return $this->ErrMsg;
			}
			$geboortedatum=$gbdat->format("Y-m-d");
		}
		//TYPO3\CMS\Core\Utility\GeneralUtility::devLog("geb dat:".$geboortedatum,"churchpersreg");
		if (empty($person['vadernaam']))
			$id_vader='';
		else
			$id_vader=intval($person['id_vader']);
		if (empty($person['moedernaam']))
			$id_moeder='';
		else
			$id_moeder=intval($person['id_moeder']);

		$id_partner="";
		if (!empty($person['partnernaam'])&&($person['burgerlijke_staat']=='gehuwd'||$person['burgerlijke_staat']=='samenwonend'))
		{	$id_partner=intval($person['id_partner']);
		}
		$trouwdatum="";
		if ($person['burgerlijke_staat']=='gehuwd')
		{	
			if (!empty($person['trouwdatum']))
			{
				$trdat=new \DateTime($person['trouwdatum']);
				if (!is_object($trdat))
				{
					$this->ErrMsg="trouwdatum ".$person['trouwdatum']." is een ongeldige datum";
					return $this->ErrMsg;
				}
				if ($trdat > new \DateTime())
				{
					$this->ErrMsg="trouwdatum kan niet in de toekomst liggen";
					return $this->ErrMsg;
				}
				if ($trdat < new \DateTime("- 100 years"))
				{
					$this->ErrMsg="trouwdatum - ".$person['trouwdatum']." - te ver in het verleden";
					return $this->ErrMsg;
				}
				$trouwdatum=$trdat->format("Y-m-d");
			}
		}
		$datumoverlijden="";
		if ($person['burgerlijke_staat']=='overleden')
		{	
			if (!empty($person['datumoverlijden']))
			{
				$ovdat=new \DateTime($person['datumoverlijden']);
				if (!is_object($ovdat))
				{
					$this->ErrMsg="datumoverlijden ".$person['datumoverlijden']." is een ongeldige datum";
					return $this->ErrMsg;
				}
				if ($ovdat > new \DateTime())
				{
					$this->ErrMsg="datumoverlijden kan niet in de toekomst liggen";
					return $this->ErrMsg;
				}
				if ($ovdat < new \DateTime("- 1 years"))
				{
					$this->ErrMsg="datumoverlijden - ".$person['datumoverlijden']." - te ver in het verleden";
					return $this->ErrMsg;
				}
				$datumoverlijden=$ovdat->format("Y-m-d");
			}
		}
		$liddatum="";
		if ($person['lid']=='is lid')
		{	
			if (!empty($person['datum_lidmaatschap']))
			{
				$lddat=new \DateTime($person['datum_lidmaatschap']);
				if (!is_object($lddat))
				{
					$this->ErrMsg="datum_lidmaatschap ".$person['datum_lidmaatschap']." is een ongeldige datum";
					return $this->ErrMsg;
				}
				if ($lddat > new \DateTime())
				{
					$this->ErrMsg="datum_lidmaatschap kan niet in de toekomst liggen";
					return $this->ErrMsg;
				}
				if ($lddat < new \DateTime("- 100 years"))
				{
					$this->ErrMsg="datum_lidmaatschap - ".$person['datum_lidmaatschap']." - te ver in het verleden";
					return $this->ErrMsg;
				}
				$liddatum=$lddat->format("Y-m-d");
			}
		}
		$doopdatum="";
		if ($person['gedoopt']=='is gedoopt')
		{	
			if (!empty($person['doopdatum']))
			{
				$dpdat=new \DateTime($person['doopdatum']);
				if (!is_object($dpdat))
				{
					$this->ErrMsg="doopdatum ".$person['doopdatum']." is een ongeldige datum";
					return $this->ErrMsg;
				}
				if ($dpdat > new \DateTime())
				{
					$this->ErrMsg="doopdatum kan niet in de toekomst liggen";
					return $this->ErrMsg;
				}
				if ($dpdat < new \DateTime("- 100 years"))
				{
					$this->ErrMsg="doopdatum - ".$person['doopdatum']." - te ver in het verleden";
					return $this->ErrMsg;
				}
				$doopdatum=$dpdat->format("Y-m-d");
			}
		}
		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		
		//first handle address
		$this->ErrMsg=$this->SaveAddress($person,$userid);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'persoon na save address: '.str_replace('=','="',urldecode(http_build_query($person,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');

		//$liddatum=church_div::convertdateToDB($person['datum_lidmaatschap']);
		$fieldarray=array('achternaam'=>$this->db->escape_string(ucwords($person['achternaam'],"\t\r\n\f\v\-")),'tussenvoegsel'=>$this->db->escape_string($person['tussenvoegsel']),
		'voornamen'=>$this->db->escape_string(ucwords($person['voornamen'],"\t\r\n\f\v\-")),'roepnaam'=>$this->db->escape_string(ucfirst($person['roepnaam'])),
		'geslacht'=>(empty($person['geslacht'])?'':$person['geslacht']),
		'beroep'=>$this->db->escape_string($person['beroep']),'id_adres'=>"AES_ENCRYPT('".$person['id_adres']."',@password)",
		'mobieltelnr'=>"AES_ENCRYPT('".$person['mobieltelnr']."',@password)",
		'mobiel_op_website'=>(!empty($person['mobiel_op_website']) and $person['mobiel_op_website']=='mobiel tonen'?"mobiel tonen":"mobiel niet tonen"),
		'emailadres'=>"AES_ENCRYPT('".$person['emailadres']."',@password)",
		'email_op_website'=>(!empty($person['email_op_website']) and $person['email_op_website']=='email tonen'?"email tonen":"email niet tonen"),
		'geboortedatum'=>$geboortedatum,'geboorteplaats'=>$this->db->escape_string($person['geboorteplaats']),
		'geboortedatum_op_intranet'=>(empty($person['geboortedatum_op_intranet']))?"0":'1',
		'id_vader'=>$id_vader,'id_moeder'=>$id_moeder,
		'burgerlijke_staat'=>$this->db->escape_string($person['burgerlijke_staat']),
		'id_partner'=>$id_partner,
		'bezoeker'=>(empty($person['bezoeker']))?"is geen bezoeker":$this->db->escape_string($person['bezoeker']),
		'lid'=>(empty($person['lid']))?"is geen lid":$this->db->escape_string($person['lid']),
		'datum_lidmaatschap'=>$liddatum,
		'gedoopt'=>(empty($person['gedoopt']))?"niet gedoopt":$this->db->escape_string($person['gedoopt']),
		'doopdatum'=>$doopdatum,
		'doopplaats'=>$this->db->escape_string($person['doopplaats']),'doopgemeente'=>$this->db->escape_string($person['doopgemeente']),
		);
		//if (!empty($trouwdatum))$fieldarray['trouwdatum'] =$trouwdatum;
		if ($person['burgerlijke_staat']=='gehuwd' and !empty($trouwdatum)) $fieldarray['trouwdatum'] = $trouwdatum;
		if ($person['burgerlijke_staat']=='overleden' and !empty($datumoverlijden)) $fieldarray['datumoverlijden'] = $datumoverlijden;
//		$nonquotearray=array("id_adres","emailadres","mobieltelnr");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'fieldarray: '.urldecode(http_build_query($fieldarray,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		
		if ($this->cleanteamReg or empty($person['bezoeker']))
				$fieldarray['cleanteam']=(empty($person['cleanteam']) or empty($person['bezoeker']))?"kan niet meedoen":$person['cleanteam'];
		else $fieldarray['cleanteam']="kan meedoen";
		
		
		if (empty($person_id))$fieldarray['invoerdatum']=date('Y-m-d');
		else
		{
			if ($this->cleanteamReg){
				// get old 'cleanteam' field:
				
				$this->query->statement('select cleanteam,bezoeker from persoon where uid="'.$person_id.'"');
        		$results= $this->query->execute(true);
				$cleanteam=$results[0]['cleanteam'];
				$bezoeker=$results[0]['bezoeker'];
			}
			if ($this->cleanteamReg)
			{
				if ($cleanteam=='kan niet meedoen' and $fieldarray['cleanteam']=='kan meedoen' or $bezoeker=="is geen bezoeker" and $fieldarray['bezoeker']=='is bezoeker')
				{
					$this->AddtoCleanteam($person_id,$geboortedatum,$person['id_adres']);
					$fieldarray['cleanteam']='kan meedoen';
				}
				else if ($cleanteam=='kan meedoen' and $fieldarray['cleanteam']=='kan niet meedoen' or $bezoeker=="is bezoeker" and $fieldarray['bezoeker']=='is geen bezoeker')
				{
					// verwijder ook cleanteam-taakbekleding:
					$result=$this->db->query("delete from taakbekleding where id_persoon='".$person_id."' and id_parent in (select uid from taak where omschrijving like 'cleanteam%%')");
				}
			}
		} 
		$fields='';
		$st='';
		foreach ($fieldarray as $key => $value) {
			$fields.=$st.$key.'=';
			if (substr($value,0,11)=='AES_ENCRYPT') $fields.=$value; else  $fields.='"'.$value.'"';
			$st=',';
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'persoon fields: '.$fields."; person_id:".$person_id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		if (empty($person_id))
		{	
			// check if person has already been entered:
			$statement='select datum_wijziging from persoon where AES_DECRYPT( id_adres ,@password)='.$person['id_adres'].' and achternaam="'.$this->db->escape_string($person['achternaam']).
			'" and voornamen="'.$this->db->escape_string($person['voornamen']).'" and geslacht="'.(empty($person['geslacht'])?'':$person['geslacht']).'" and geboortedatum="'.$person['geboortedatum'].'" and deleted=0';
			$result=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'insert persoon check duplicate: '.$statement."; error:".$this->db_error."; aantal:".$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			if (!$result or $result->num_rows==0)
			{
				// not found
				$fields.=',steller="'.$userid.'",';

				$statement='insert into persoon set '.$fields.'datum_wijziging=NOW()';
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'insert persoon: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				if ($this->db->query($statement))
				{
					$person_id=$this->db->insert_id;
					$person['person_uid']=$person_id;
					//$results= $this->query->execute();
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'inserted person id: '.$person_id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				
					if ($fieldarray['cleanteam']=="kan meedoen") $this->AddtoCleanteam($person_id,$geboortedatum,$person['id_adres']); 
				}
				else 
				{
					$this->ErrMsg="insert person wrong:".$this->db->error;
				//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'inserted person fout: '.$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
					
				}
			}
		}
		else
		{
			$statement='update persoon set '.$fields.' where uid="'.$person_id.'"';
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'update persoon: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			
			$results=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'update persoon: '.$statement.'; affected:'.$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			if ($this->db->affected_rows >0 and !(int)churchpersreg_div::Heeftpermissie(churchpersreg_div::ADMINFUNC))
			{
				//something changed:
				$statement='update persoon set steller="'.$userid.'",datum_wijziging=NOW() where uid="'.$person_id.'"';
				$results=$this->db->query($statement);
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'pas relatie met partner aan burgerlijke_staatorg: '.$person['burgerlijke_staatorg'].'; burgerlijke_staat:'.$person['burgerlijke_staat'].'; partnerorg:'.$person['partnerorg']."; id_partner:".$id_partner."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				if ($person['burgerlijke_staat']=='overleden' && $person['burgerlijke_staat']!=$person['burgerlijke_staatorg'])
				{
					if ($person['burgerlijke_staatorg']=='gehuwd' && !empty($person['partnerorg']))
					{
						// change state of partner to 'weduwnaar':
						$statement='update persoon set burgerlijke_staat=if(geslacht="vrouw","weduwe","weduwnaar"),steller="'.$userid.'",datum_wijziging=NOW()  where uid='.$person['partnerorg'];
						$result=$this->db->query($statement);
					}
					elseif ($person['burgerlijke_staatorg']=='samenwonend' && !empty($person['partnerorg']))
					{
						// change state of partner to 'ongehuwd':
						$statement='update persoon set burgerlijke_staat="ongehuwd",id_partner="",steller="'.$userid.'",datum_wijziging=NOW() where uid='.$person['partnerorg'];
						$result=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'update samenwonend partner bij overlijden: '.$statement.'; affected:'.$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
					}
					// verwijder taken:
					$statement="delete from taakbekleding where id_persoon =".$person_id;
					$results=$this->db->query($statement);
					// verwijder email adres:
					$statement='update persoon set emailadres=""  where uid='.$person_id;
					$result=$this->db->query($statement);

				}
			}
				
			// Eventueel oude partner relatie verwijderen:
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'pas relatie met partner aan nw: '.$person['id_partner']."; oud:".$person['partnerorg']."; id_partner:".$id_partner."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'verwijder relatie met partner: partnerorg:'.$person['partnerorg'].'; id_partner:'.$id_partner.'.burgelijke staat: '.$person['burgerlijke_staat']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			if ($person['partnerorg']>0 and (empty($id_partner) or $id_partner<>$person['partnerorg']) and $person['burgerlijke_staat']<>'overleden')//$id_partner
			{	// verwijder eventuele relatie bij partner:
				$statement='update persoon set id_partner="",trouwdatum="",burgerlijke_staat="'.(empty($id_partner)?$person['burgerlijke_staat']:'').'" where uid='.$person['partnerorg'];
				$result=$this->db->query($statement);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'verwijder relatie met partner: '.$statement.", fout: ".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			}
		}
		if (!empty($id_partner)&&($person['burgerlijke_staat']=='gehuwd' || $person['burgerlijke_staat']=='samenwonend'))
		{	// breng relatie ook aan bij partner:
			$statement='update persoon set id_partner='.$person_id.',trouwdatum="'.$trouwdatum.'",burgerlijke_staat="'.$person['burgerlijke_staat'].'" where uid='.$id_partner;
			$result=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'update partner bij trouwen/samenwonen: '.$statement.'; affected:'.$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		}
		return $this->ErrMsg;
	}
	 /**
     * Delete function 
     *
     * param int  $cPersids                 id of persoon
	 *        int  $id_adres                 id of address
	 *        int  $huisgenoten              ids of persons connected to same address
	 *		  int $verwijderreden			 reason for deleting person
	 *		  int $removeall				 remove all people on this address
     *
     * return void
     */
    public function delete($cPersids,$id_adres,$huisgenoten,$verwijderreden,$removeall)
	{
		// initialize mysqli:
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete person start personid:'.$personid.'; id_adres:'.$id_adres."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		$steller=$GLOBALS['TSFE']->fe_user->user['person_id'];
		churchpersreg_div::connectdb($this->db);
		// determine related persons:
		$aHuisgenoten=explode(', ',$huisgenoten);
		if ($removeall && count($aHuisgenoten)>1)
		{
			$this->initializeObject();
			$query = $this->createQuery();
			$statement="select group_concat(`uid` separator ',') as persids from persoon where deleted=0 and AES_DECRYPT(id_adres,@password)=".intval($id_adres);
			$query->statement($statement);
			$aPersids=$query->execute(true);
			$cPersids=$aPersids[0]['persids'];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'find huisgenoten statement:'.$statement."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 

		}
		if ($removeall && count($aHuisgenoten)>1)$aPersonids=explode($cPersids,',');
		else $aPersonids=array($cPersids);
		// delete aanvragen:
		// Read person data:
		$this->initializeObject();
		$query = $this->createQuery();
		$statement="SELECT p.uid,date_format(geboortedatum,'%d-%m-%Y') as geboortedatum,roepnaam,achternaam,postcode,huisnummer,p.invoerdatum from persoon p,adres a where p.uid in (".$cPersids.") and AES_DECRYPT(p.id_adres,@password)=a.uid and p.deleted=0 and a.deleted=0";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete get person data: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		$query->statement($statement);
		$aPersdata=$query->execute(true);	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'to be deleted person data: '.urldecode(http_build_query($aPersdata,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 

		foreach ($aPersdata as $Persdata)
		{
			// remove aansluitingsverzoek:
			$statement='update tx_formtodatabase_domain_model_formresult set deleted=1 where deleted=0 and from_unixtime(crdate, "%Y-%d-%m")<="'.$Persdata['invoerdatum'].'" and replace(result->\'$."text-6"\',\'"\',\'\')="'.$Persdata['geboortedatum'].'"'.
			' and replace(result->\'$."text-1"\',\'"\',\'\')="'.$Persdata['roepnaam'].'"';
			$result=$this->db->query($statement);
			// remove request for membership or baptism:
			$statement='update aanvraag set deleted=1 where deleted=0 and uidpersoon="'.$Persdata['uid'].'" and datumaanvraag>"'.$Persdata['invoerdatum'].'"';
			$result=$this->db->query($statement);
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete tx_formtodatabase_domain_model_form result: '.$this->db->error.'; statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		}
		
        $statement='update persoon set deleted=1,steller='.$steller.',verwijderreden='.intval($verwijderreden).',datum_wijziging=NOW() where deleted=0 and uid in ('.$cPersids.')';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete persson statement:'.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$results=$this->db->query($statement);
		if (intval($verwijderreden)==2)
		{
			// wijzig burgelijke staat eventuele partner:
			$statement='update persoon set burgerlijke_staat=if(geslacht="man","weduwnaar","weduwe"),datum_wijziging=NOW(),steller='.$steller.' where id_partner='.$cPersids.' and deleted=0 and burgerlijke_staat="gehuwd"'; 
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'wijzig partner statement:'.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
			$results=$this->db->query($statement);		
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete person statement:'.$statement."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 

		// verwijder ook losstaand adres indien geen andere huisgenoten:
		// zijn er nog andere personen op dit adres?
		if ($removeall ) 
		{
			$statement="update adres set deleted=1,steller=".$steller." where uid=".intval($id_adres);
			$results=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete address statement:'.$statement."; error:".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		}
		// verwijder ook bijbehorende bedieningsprofiel:
		$statement="update bedieningsprofiel set deleted=1,steller=".$steller." where AES_DECRYPT(person_id,@password) in (".$cPersids.") and deleted=0";
		$results=$this->db->query($statement);
	//	$this->ErrMsg=$this->db->error;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete bedieningsprofiel: '.$statement."; error:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		// verwijder ook bijbehorende contactinfo:
		$statement="update contact_archive set deleted=1 where id_person in (".$cPersids.") and deleted=0";
		$results=$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete contact_archive: '.$statement."; error:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		// verwijder ook bijbehorende gebruikersaccount en gebruikersgroep-koppeling:
/*		$statement="delete from rm_g_gg where uid in (select uid from fe_users where person_id in (".$cPersids."))";
		$results=$this->db->query($statement); */
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete gebruikersgroep-koppeling: '.$statement."; error:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		$statement="delete from fe_users where person_id in (".$cPersids.")";
		$results=$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete feusers: '.$statement."; error:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		// verwijder ook taakbekleding:
		$statement="delete from taakbekleding where id_persoon in (".$cPersids.")";
		$results=$this->db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete taakbekleding: '.$statement."; error:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		
		// verwijder als bedieningsleider:
		foreach ($aPersonids as $personid)
		{
			$statement="update bediening set id_bedieningsleider= TRIM(BOTH ',' FROM REPLACE( CONCAT(',',id_bedieningsleider,','),',".$personid.",',',') ) where find_in_set('".$personid."',id_bedieningsleider)";
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete bedieningsleiders: '.$statement."; error:".$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
			$results=$this->db->query($statement);		
		}
		return $this->ErrMsg;
	}

	 /**
     * ContactInfoSave function 
     *
     * param $archive
     *
     * return void
     */
    public function ContactInfoSave(&$archive,$extpath)
	{
		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ContactInfoSave  archive: '.urldecode(http_build_query($archive,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$fields='id_person='.intval($archive['id_person']).',id_person_owner='.intval($archive['id_person_owner']).
		',subject=AES_ENCRYPT("'.$this->db->escape_string($archive['subject']).'",@password),date="'.$archive['date'].'"'.
		',author="'.$this->db->escape_string($archive['author']).'",confidential='.intval($archive['confidential']);
		if 	($archive['type_content']=='notitie')$fields.=',type_content="notitie",content=AES_ENCRYPT("'.$this->db->escape_string($archive['content']).'",@password)';			
		if 	($archive['type_content']=='bestand')
		{
			if (!empty($archive['document_name']["tmp_name"]) && $archive['document_name']['size']>0)
			{
				$fields.=',type_content="bestand",document_name=AES_ENCRYPT("'.$this->db->escape_string($archive['document_name']['name']).'",@password)';
				$filepath=$archive['document_name']["tmp_name"];
				$fp      = fopen($filepath, 'r');
				$content = fread($fp, filesize($filepath));
				fclose($fp);
				//unlink($filepath);  /// remove tempory file
				$fields.=',content=AES_ENCRYPT("'.$this->db->escape_string($content).'",@password)';		
			}
		} 
		if (empty($archive['uid']))
		{
			$statement="insert into contact_archive set ".$fields;
			$results=$this->db->query($statement);
			$archive['uid']=$this->db->insert_id;
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'insert contactinfo: '.$statement.", fout: ".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		}
		else 
		{
			$statement="update contact_archive set ".$fields." where uid=".intval($archive['uid']);
			$results=$this->db->query($statement);
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'update contactinfo: '.$statement.", fout: ".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		}

		return $this->ErrMsg;
	}

		 /**
     * deleteArchive function 
     *
     * param $archive
     *
     * return void
     */
    public function deleteArchive($archiveid)
	{
		// initialize mysqli:
		if (!empty($archiveid))
		{
			churchpersreg_div::connectdb($this->db);
			$statement="delete from contact_archive where uid=".intval($archiveid);
			if (!$this->db->query($statement)) $this->ErrMsg=$this->db->error;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete contactinfo: '.$statement.", fout: ".$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');

		}
		return $this->ErrMsg;
	}

	/**
     * Select required transport function 
     *
     * param $archive
     *
     * return void
     */
    public function selectTransport($aantalgevonden)
	{
		// synchronise localconf with database:
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
		$aValues = array_filter($extensionConfiguration, function ($value,$key) {
			return (substr($key,0,19)=='transport_smtp_user' && !empty($value));
		}, ARRAY_FILTER_USE_BOTH);
		if (empty($aValues)) return;
		// remove redundant transport users:
		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		$users='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectTransport aValues: '.urldecode(http_build_query($aValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		
		foreach ($aValues as $value)
		{
			if (!empty($users))$users.=',';
			$users.='"'.(explode("#$#",$value))[0].'"';
		}
		$stmnt='delete from transportusername where user_name not in ('.$users.')';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete transport stmnt: '. $stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->db->query($stmnt);

		$stmnt='insert into transportusername (user_name, user_password) values ("'.implode('"),("',str_replace('#$#','","',$aValues)).'") on duplicate key update user_password = values(user_password)';	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'insert transport stmnt: '. $stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->db->query($stmnt);
		

		// Determine preferrable transport:
		$query="Select uid,user_name,user_password,date_used,if(date_used < DATE_SUB(NOW(), INTERVAL 25 HOUR),0,totalsent) as totalsent from transportusername WHERE date_used < DATE_SUB(NOW(), INTERVAL 25 HOUR) or totalsent < (950 - ".$aantalgevonden." ) order by date_used ,totalsent asc limit 1";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'select transport query: '. $query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$rows=$this->db->query($query);
		if ($rows!==false and $rows->num_rows>0)
		{
			$transport=$rows->fetch_array(MYSQLI_ASSOC);
			$transport_smtp_username=$transport['user_name'];
			$transport_smtp_password=$transport['user_password'];
			$totalsent=$transport['totalsent']+$aantalgevonden;
			$stmnt="update transportusername set date_used= CURRENT_TIMESTAMP,totalsent= ".$totalsent." where uid=".$transport['uid'];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'update transport : '. $stmnt.'; transport_smtp_username:'.$transport_smtp_username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			$this->db->query($stmnt);
			$LocalConfiguration = GeneralUtility::makeInstance(ConfigurationManager::class);
			$LocalConfiguration->setLocalConfigurationValueByPath('MAIL/transport_smtp_username', $transport_smtp_username);
			$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username']=$transport_smtp_username;
			if (!empty($transport_smtp_password)) 
			{
				$LocalConfiguration->setLocalConfigurationValueByPath('MAIL/transport_smtp_password', $transport_smtp_password);
				$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_password']=$transport_smtp_password;
			}
		} 

		return;
	}
	
	/**
	 * Method 'GetPersons' for the 'churchregpers' extension.
 	*
	 * param searchfield: keyword required persons
	 * returns string with select statement 
	 */
	function GetPersons(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//	$this->ErrMsg="Getpersons zoekveld 1:".$Zoekveld_1.",2:".$Zoekveld_2.",3:".$Zoekveld_3.",4:".$Zoekveld_4.",5:".$Zoekveld_5.",6:".$Zoekveld_6;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons:bezoeker: '.$Bezoeker."\r\n",3,'churchpersreg_pi3.txt');

		$zoekargument=$this->ComposeSearch($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons:bezoeker: '.$Bezoeker."; zoekargument:".$zoekargument."\r\n",3,'churchpersreg_pi3.txt');
		if (empty($search['peradres']))$order='tp.achternaam';else $order='postcode,huisnummer,tp.burgerlijke_staat DESC, tp.geslacht ASC';
		if ((isset($search['peradres']))?'tp.achternaam':'postcode,huisnummer,tp.burgerlijke_staat DESC, tp.geslacht ASC')
		/* Performing SQL query */ 
		$statement = 'SELECT tp.uid as person_uid'.
		', trim(replace(concat(tp.roepnaam,if(tp.tussenvoegsel="",""," "),tp.tussenvoegsel," ",tp.achternaam),"  "," ")) as naam, tp.geslacht'.
		', tp.gebruik_naam_partner, tp.achternaam,trim(tp.roepnaam) as roepnaam,trim(tp.tussenvoegsel) as tussenvoegsel,tp.id_partner'.
		', trim(replace(concat(tp.tussenvoegsel,if(tp.tussenvoegsel="",""," "),tp.achternaam),"  "," ")) as lastname'.
		', concat(tpartner.roepnaam,if(tpartner.tussenvoegsel="",""," "),tpartner.tussenvoegsel," ",tpartner.achternaam) as partnernaam'.
		', straatnaam,huisnummer,postcode,woonplaats,land'.
		', AES_DECRYPT(telefoonnr_vast,@password) as telefoonnr_vast'.
		', AES_DECRYPT(tp.emailadres,@password) as emailadres'.
		', tp.email_op_website, tel_op_website,adres_op_website, tp.mobiel_op_website'.
		', AES_DECRYPT( tp.mobieltelnr ,@password) as mobieltelnr '.
		', tp.id_vader,tp.id_moeder'.
		', tp.foto="" or tp.foto is null as nofoto '.
		', tp.photoWidth, tp.photoHeight '.
		', tp.deleted '.
		', ta.uid as aid'. 
		' FROM persoon as tp'.
		' left join persoon as tpartner on tp.id_partner=tpartner.uid,adres as ta'.
		' where '.$zoekargument.
		' ORDER BY '.$order; 

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons: statement: '.$statement."; zoekargument:".$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		return ($statement);
	}
	
	/**
	 * Method 'GetAddresses' for the 'churchpersreg' extension.
 	*
	 * param searchfield: keyword required addresses with persons
	 * returns string with statement  
	 */
	function GetAddresses(array $search = null)
	{					
	//	Samenstellen van zoekargument
		$zoekargument=$this->ComposeSearch($search);
		
		/* Performing SQL query */ 
		$statement = 'SELECT group_concat(tp.uid,"#",concat_ws(" ",tp.roepnaam,tp.tussenvoegsel,tp.achternaam),"#",tp.deleted,"#",tp.gedoopt,"#",tp.lid separator ", ") as huisgenoten '.
		', straatnaam,huisnummer,postcode,woonplaats,land'.
		', AES_DECRYPT(telefoonnr_vast,@password) as telefoonnr_vast'.
		' FROM 	adres as ta'.
		' ,persoon as tp left join persoon as tpartner on tp.id_partner=tpartner.uid'.
		' where '.$zoekargument.
		' GROUP BY ta.uid ORDER BY postcode,huisnummer,tp.burgerlijke_staat DESC, tp.geslacht ASC'; 
		return ($statement);
	}

	/**
	 * Method 'GetExportPersons' for the 'churchregpers' extension.
 	*
	 * param searchfield: keyword required persons
	 * returns string with select statement 
	 */
	function GetExportPersons(array $search = null)
	{	
	//	Samenstellen van zoekargument

		$zoekargument=$this->ComposeSearch($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetPersons:bezoeker: '.$Bezoeker."; zoekargument:".$zoekargument."\r\n",3,'churchpersreg_pi3.txt');
		$personreg=(int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon Schrijven");
		$userlid= $GLOBALS['TSFE']->fe_user->user['lid'];
		$statement = 'SELECT '.
		' tp.achternaam, tp.tussenvoegsel, tp.voornamen, tp.roepnaam'.
		', straatnaam,huisnummer,postcode,woonplaats,land,DATE_FORMAT(ta.datum_wijziging,GET_FORMAT(DATE,"ISO")) as datum_adreswijziging'.
		', AES_DECRYPT(telefoonnr_vast,@password) as telefoonnr_vast'.
		', AES_DECRYPT(tp.mobieltelnr,@password) as mobieltelnr'.
		', AES_DECRYPT(tp.emailadres,@password) as emailadres'.
		', tp.geslacht, tp.geboortedatum'.(($personreg)?',tp.geboorteplaats':'').
		', concat(tvader.roepnaam," ",tvader.tussenvoegsel," ",tvader.achternaam) as vader'.
		', concat(tmoeder.roepnaam," ",tmoeder.tussenvoegsel," ",tmoeder.achternaam) as moeder'.
		(($personreg)?', tp.burgerlijke_staat, tp.trouwdatum,tp.datumoverlijden,tp.datum_wijziging':'').
		(($search['inclverwijderd'])?',if(tp.deleted=1,"ja","nee") as verwijderd,
		if(tp.deleted=1,case tp.verwijderreden
        when 0 then "onbekend"
        when 1 then "verhuisd"
        when 2 then "Overleden"
        when 3 then "Onvrede"
        when 5 then "Overgestapt naar andere gemeente"
        when 6 then "Geloof  kwijtgeraakt"
        when 7 then "Afgehaakt"
 end  
 ,"") as redenverwijderd':'').
		', concat(tpartner.roepnaam," ",tpartner.tussenvoegsel," ",tpartner.achternaam) as partner'.
		(($userlid)?', tp.gedoopt'.(($personreg)?',tp.doopdatum,tp.doopplaats,tp.doopgemeente':'').
		', tp.lid'.(($personreg)?', tp.datum_lidmaatschap, tp.invoerdatum':''):'').
		' FROM persoon as tp'.
		' LEFT JOIN persoon as tpartner ON tp.id_partner=tpartner.uid '.
		' LEFT JOIN persoon as tvader ON tp.id_vader=tvader.uid '.
		' LEFT JOIN persoon as tmoeder ON tp.id_moeder=tmoeder.uid, adres as ta'.
		' where '.$zoekargument.
		' ORDER BY '.(($search['peradres'])?'postcode,huisnummer,tp.burgerlijke_staat DESC, tp.geslacht ASC':'tp.achternaam'); 
		return ($statement);
		
		/*
		: reden vertrek:
		SELECT `uid`,`roepnaam`,`tussenvoegsel`,`achternaam`,`datum_wijziging`,
case `verwijderreden`
        when 0 then 'onbekend'
        when 1 then 'verhuisd'
        when 2 then 'Overleden'
        when 3 then 'Onvrede'
        when 5 then 'Overgestapt naar andere gemeente'
        when 6 then 'Geloof  kwijtgeraakt'
        when 7 then 'Afgehaakt'
 end  
 as reden  
FROM `persoon` where `deleted`=1 and DATE( FROM_UNIXTIME( unix_timestamp(`datum_wijziging`) ) ) between "2020-09-01" and "2021-08-31"
		*/
	}

	/**
	 * Method 'ComposeSearch' for the 'churchpersreg' extension.
 	*
	 * param searchfield: keyword required persons ({$Zoekveld,$Zoekveld_1,$Zoekveld_2,$Zoekveld_3,$Zoekveld_4,$Zoekveld_5,$Zoekveld_6})
	 * returns composed sql search parameter
	 */
	function ComposeSearch(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//			$this->ErrMsg.="Compose zoekveld 1:".$Zoekveld[1].",2:".$Zoekveld[2].",3:".$Zoekveld[3].",4:".$Zoekveld[4].",5:".$Zoekveld[5].",6:".$Zoekveld[6].",7:".$Zoekveld[7];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ComposeSearch search : '.urldecode(http_build_query($search,NULL,"=")).'; typerequest: '.$search['typerequest']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$zoekargument="";
		$Not=false;
		$conneRction=\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getConnectionForTable('string');
	/*	if (!empty($Zoekveld[7])){
			$zoekargument .=  "(tp.uid = '" . addslashes($Zoekveld[7]) ."') AND ";}		*/
		if (!empty($search['achternaam'])){
			$search['achternaam']=trim($search['achternaam']);
			$zoekargument .=  "(concat_ws(' ',tp.tussenvoegsel,tp.achternaam) LIKE '%" . addslashes($search['achternaam']) ."%' OR concat_ws(' ',tpartner.achternaam,tpartner.tussenvoegsel) LIKE '%" . addslashes($search['achternaam']) ."%') AND ";}
		if (!empty($search['voornaam'])){ 
			$search['voornaam']=trim($search['voornaam']);
			$zoekargument .=  "(tp.roepnaam LIKE '%" . addslashes($search['voornaam']) ."%' OR tp.voornamen LIKE '%" . addslashes($search['voornaam']) ."%') AND ";}
		if (!empty($search['postcode'])){ 
			$zoekargument .=  "ta.postcode LIKE '%" . addslashes($search['postcode']) ."%' AND ";}
		if (!empty($search['woonplaats'])){ 
			$search['woonplaats']=trim($search['woonplaats']);
			$zoekargument .=  "ta.woonplaats LIKE '%" . addslashes($search['woonplaats']) ."%' AND ";}
		if (!empty($search['leeftijdmin'])){ 
			$zoekargument .=  "adddate(tp.geboortedatum,interval " . $search['leeftijdmin'] ." year)<=curdate() AND ";}
		if (!empty($search['leeftijdmax'])){ 
			$zoekargument .=  "adddate(tp.geboortedatum,interval " . $search['leeftijdmax'] ."+1 year)>curdate() AND ";}
		if (!empty($search['emailadres'])){ 
			$zoekargument .=  "AES_DECRYPT(tp.emailadres,@password) LIKE '%" . $search['emailadres'] ."%' COLLATE UTF8_GENERAL_CI AND ";}
		if (!empty($search['general'])){ 
			$a_key=$this->DeriveKeywords($search['general']);
				//	TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekveld:".$Zoekveld[0],"churchpersreg");
			foreach ($a_key as $key)
			{	$key=trim($key);
				if (!empty($key))
				{	// key:
					if (substr($key,0,1)=="-")
					{	
						$zoekargument.="NOT ";  // ontkenning
						$key=substr($key,1);
						$Not=true;
					}			
					$key=addslashes($key);
					//$keyphone1=preg_replace("/^31/","0",$key);
					$keyphone=preg_replace(array('/(\s|-)/','/^31/'),array("","0"),$key);
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ComposeSearch search:'.$search['general'].'; key:'.$key.';  keyphone : '.$keyphone."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
					$zoekargument.=' (concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) like "%'.$key.'%" OR ';
					// ook op andere velden zoeken:
					$zoekargument .=  "tp.voornamen LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "straatnaam LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "huisnummer LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "postcode LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "woonplaats LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "land LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.beroep LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.geslacht LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tp.burgerlijke_staat LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.geboorteplaats LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.gedoopt LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tp.doopplaats LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.doopgemeente LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.lid LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tp.cleanteam LIKE '" . $key ."%' OR ";
					$zoekargument .=  "AES_DECRYPT(tp.emailadres,@password) LIKE '%" . $key ."%' COLLATE UTF8_GENERAL_CI OR ";
					$zoekargument .=  "tp.email_op_website LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tp.bezoeker LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tp.mobiel_op_website LIKE '" . $key ."%' OR ";
					$zoekargument .=  "adres_op_website LIKE '" . $key ."%' OR ";
					$zoekargument .=  "tel_op_website LIKE '" . $key ."%' OR ";
					$zoekargument .=  "REPLACE(AES_DECRYPT(tp.mobieltelnr,@password),'-','') LIKE '%" . $keyphone ."%' OR ";
					$zoekargument .=  "tp.uid in (select tb.id_persoon from taakbekleding tb, taak tk, bediening b where tb.id_parent=tk.uid and tk.id_parent=b.uid and (tk.omschrijving like '" . $key ."%' or b.omschrijving like '" . $key ."%')) OR ";
					$zoekargument .=  "(tpartner.achternaam like '%". $key ."%' AND tpartner.achternaam IS NOT NULL) OR ";
					$zoekargument .=  "(tpartner.roepnaam like '%". $key ."%' AND tpartner.roepnaam IS NOT NULL) OR ";
					$zoekargument .=  "(REPLACE(AES_DECRYPT(telefoonnr_vast,@password),'-','') LIKE '%" . $keyphone ."%' and AES_DECRYPT(telefoonnr_vast,@password) IS NOT NULL) ) and ";
				}
			}
		}
		$personreg= (int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon Schrijven");
		if (isset($search['bezoeker']))$zoekbezoeker=1; else $zoekbezoeker=0;
		if (!$personreg || $zoekbezoeker)$zoekargument.=" tp.bezoeker='is bezoeker' and ";
		$zoekargument.=' AES_DECRYPT(tp.id_adres,@password)=ta.uid';
		if (!empty($search['bezoeker']))
		{
			if(!empty($search['inputdatmin']))$zoekargument.=' and tp.invoerdatum>="'.$search['inputdatmin'].'"';
			if(!empty($search['inputdatmax']))$zoekargument.=' and tp.invoerdatum<"'.$search['inputdatmax'].'"';
		}
		if (!empty($search['lid']))
		{
			$zoekargument.=" and tp.lid='is lid'";
			if(!empty($search['liddatmin']))$zoekargument.=' and tp.datum_lidmaatschap>="'.$search['liddatmin'].'"';
			if(!empty($search['liddatmax']))$zoekargument.=' and tp.datum_lidmaatschap<"'.$search['liddatmax'].'"';
		}
		if (empty($search['inclverwijderd']) or !churchpersreg_div::Heeftpermissie("Verwijderde lezen"))$zoekargument.=' and tp.deleted=0 and ta.deleted=0'; // only active persons
		if (!empty($search['inclverwijderd']) and $search['inclverwijderd']==2 and churchpersreg_div::Heeftpermissie("Verwijderde lezen"))$zoekargument.=' and tp.deleted=1'; //only removed persons
		if (!empty($search['inclverwijderd']))
		{
			if(!empty($search['wijzdatmin']))$zoekargument.=' and (tp.datum_wijziging>="'.$search['wijzdatmin'].'" or tp.deleted=0)';
			if(!empty($search['wijzdatmax']))$zoekargument.=' and (tp.datum_wijziging<"'.$search['wijzdatmax'].'"  or tp.deleted=0)';
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ComposeSearch zoekargument : '.$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		
//		$this->ErrMsg.=",br>Zoekargument:".$zoekargument;
		return $zoekargument;
	}
	
	/*
	 * function DeriveKeywords:
	 * Derive search keywords from a Google-like searchfield $strZoek
	 */
	function DeriveKeywords($strZoek){
		// bepalen van zoekwoorden uit een vrije tekst veld
		$aZoekwoord=array();
		$aZoekveld=array();
		$Quote="";
		$WordQ="";
		$Min="";
		// Replace &quote; back to ":
		$strZoek=str_ireplace("&quot;", '"', $strZoek);
		// splits in losse woorden:
		$aZoekwoord = preg_split ("/[\s,;]{1,}/", trim($strZoek)); 
		// voeg strings tussen quote's weer samen:
		for ($i=0;$i<count($aZoekwoord);++$i)
		{	$Woord=$aZoekwoord[$i];
			if (empty($Quote))
			{	// zoeken naar quote:
				// eerst checken op +-
				if (substr($Woord,0,1)=="-")
				{	$Min="-";
					$Woord=substr($Woord,1);
				}
				else
				{	$Min="";
					if (substr($Woord,0,1)=="+"){$Woord=substr($Woord,1);} //+ schrappen
				}
					
				if (substr($Woord,0,1)=='"' or substr($Woord,0,1)=="'")
				{	$Quote=substr($Woord,0,1);
					$aZoekwoord[$i]=$Min.substr($Woord,1);
					$Min="";
					--$i; // zoekwoord nogmaals meenemen voor verwerking
				}
				else
				{	array_push($aZoekveld,addslashes($Min.$Woord));
				}
			}
			else
			{	// zoeken naar eind quote:
				if (substr($Woord,strlen($Woord)-1,1)==$Quote)
				{	$WordQ.=" ".substr($Woord,0,strlen($Woord)-1);
					array_push($aZoekveld,trim(addslashes($WordQ)));
					$WordQ="";
					$Quote="";
				}
				else
				{	$WordQ.=" ".$Woord;
				}
			}
		}
		if (!empty($WordQ)){array_push($aZoekveld,$WordQ);} // laatste tot eind ook meenemen
		return $aZoekveld;
	}
	

	
	/**
	 * Method 'GetPerson' for the 'churchpersreg' extension.
 	*
	 * param person_id: uid of selected person
	 * returns associative array with fields of the found person 
	 */
	function GetPerson($person_id)
	{	
		$statement = 'SELECT DISTINCT tp.uid as person_uid'.
		', concat(tp.roepnaam,if(tp.tussenvoegsel="",""," "),tp.tussenvoegsel," ",tp.achternaam) as naam,tp.geslacht'.
		', tp.roepnaam,tp.tussenvoegsel,tp.achternaam,tp.voornamen'.
		', straatnaam,huisnummer,postcode,woonplaats,land,replace(huisgenoten,"  "," ") as huisgenoten,ta.id_adres'.
		', telefoonnr_vast'.
		', AES_DECRYPT(tp.emailadres,@password) as emailadres'.
		', tel_op_website,adres_op_website'.
		', AES_DECRYPT( tp.mobieltelnr ,@password) as mobieltelnr '.
		', tp.beroep, tp.geslacht, tp.burgerlijke_staat, tp.gebruik_naam_partner, tp.geboortedatum,tp.geboorteplaats,tp.geboortedatum_op_intranet,tp.trouwdatum, tp.id_partner,tp.datumoverlijden'.
		', tp.email_op_website, tp.bezoeker,tp.mobiel_op_website'.
		', tp.gedoopt,tp.doopdatum,tp.doopplaats,tp.doopgemeente'.
		', tp.lid, tp.datum_lidmaatschap, tp.invoerdatum,tp.cleanteam,tp.deleted'.
		', tp.id_vader,tp.id_moeder,tp.id_partner'.
		', (tp.foto="" or tp.foto is null) as nofoto '.
		', tp.photoWidth, tp.photoHeight '.
		', tp.steller, UNIX_TIMESTAMP(tp.datum_wijziging) as datum_wijziging'.
		', concat(tpartner.roepnaam,if(tpartner.tussenvoegsel="",""," "),tpartner.tussenvoegsel," ",tpartner.achternaam) as partnernaam,tpartner.deleted as partnerdeleted'.
		', concat(tvader.roepnaam,if(tvader.tussenvoegsel="",""," "),tvader.tussenvoegsel," ",tvader.achternaam) as vadernaam,tvader.deleted as vaderdeleted'.
		', concat(tmoeder.roepnaam,if(tmoeder.tussenvoegsel="",""," "),tmoeder.tussenvoegsel," ",tmoeder.achternaam) as moedernaam,tmoeder.deleted as moederdeleted'.
		', concat(tsteller.roepnaam,if(tsteller.tussenvoegsel="",""," "),tsteller.tussenvoegsel," ",tsteller.achternaam) as stellernaam'.
		', concat(coalesce(tkn.taken,""),if(tkn.taken is null,"",if(ldr.leider is null,"",",")),coalesce(ldr.leider,""),if(tkn.taken is null and ldr.leider is null,"",if(scr.secretaris is null,"",",")),coalesce(scr.secretaris,"") ) as taken'. 
		', "0" as moveall, tp.verwijderreden'.
		' FROM (select group_concat(b.uid,if(b.soort="afdeling","_a#","_b#"),concat("leider ",omschrijving) separator ",") as leider from bediening b where find_in_set("'.$person_id.'",id_bedieningsleider) ) as ldr,'.
		' (select group_concat(bs.uid,if(bs.soort="afdeling","_a#","_b#"),concat("secretaris ",bs.omschrijving) separator ",") as secretaris from bediening bs where find_in_set("'.$person_id.'",bs.secretariaat) ) as scr,'.
		' persoon as tp'.
		' LEFT JOIN persoon as tpartner ON (tp.id_partner=tpartner.uid '.(!churchpersreg_div::Heeftpermissie("Verwijderde lezen")?' and tpartner.deleted=0':'').')'.
		' LEFT JOIN persoon as tvader ON (tp.id_vader=tvader.uid '.(!churchpersreg_div::Heeftpermissie("Verwijderde lezen")?' and tvader.deleted=0':'').')'.
		' LEFT JOIN persoon as tmoeder ON (tp.id_moeder=tmoeder.uid '.(!churchpersreg_div::Heeftpermissie("Verwijderde lezen")?' and tmoeder.deleted=0':'').')'.
		' left join persoon as tsteller on tp.steller=tsteller.uid '.
		' left join (select group_concat(med.uid,"_m#",tk.omschrijving order by tk.omschrijving asc separator ",") as taken,med.id_persoon,med.uid from taakbekleding med left join taak tk on (tk.uid = med.id_parent) where med.id_persoon="'.$person_id.'") as tkn on (tkn.id_persoon=tp.uid) '.
		' right join (select straatnaam,huisnummer,postcode,woonplaats,land,ap.uid as id_adres, AES_DECRYPT(telefoonnr_vast,@password) as telefoonnr_vast,'.
		' tel_op_website,adres_op_website,group_concat(roepnaam separator ", ") as huisgenoten from adres ap,persoon pa'. 
		' where AES_DECRYPT(pa.id_adres,@password)=ap.uid and (pa.deleted=0 or pa.uid='.intval($person_id).') group by pa.id_adres) as ta on (AES_DECRYPT(tp.id_adres,@password)=ta.id_adres)'.  
		' where tp.uid="'.$person_id.'"'.(!churchpersreg_div::Heeftpermissie("Verwijderde lezen")?' and tp.deleted=0':'');

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'own getperson: '.$statement ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
//		' tel_op_website,adres_op_website,tel_op_adreslijst,group_concat(roepnaam separator ",") as huisgenoten from adres ap,persoon pa'. 
		return $statement;
	}
	
		
	/**
	 * Method 'GetChilds' for the 'churchpersreg' extension.
 	*
	 * param person_id: uid of selected person
	 * returns associative array with fields of the found childern of the person 
	 */
	function GetChilds($person_id)
	{
		$persoonschrijven=churchpersreg_div::Heeftpermissie("Persoon schrijven");	
		$statement = 'SELECT group_concat(uid,"#", concat(roepnaam,if(tussenvoegsel="",""," "),tussenvoegsel," ",achternaam),"#",deleted separator ",") as childs '.
		' from persoon where (id_vader='.$person_id.' or id_moeder='.$person_id.')';
		if (!churchpersreg_div::Heeftpermissie("Persoon schrijven")) $statement .= ' and deleted=0';
		return $statement;
		
	}

	
	/**	 * Method 'AddtoCleanteam' for the 'churchpersreg' extension.
 	*
	 * param: person_id: uid of selected person
	 *         geboortedatum : geboortedatum in "Y-m-d" format
	 *         id_adres: adres of corresponding adrdress
	 * add person to corresponding cleanteam 
	 */
	function AddtoCleanteam($person_id,$geboortedatum,$id_adres)
	{
		// add person to cleanteam:
		$jaaroud=(date('Y')-79).'-09-01';
		$jaarjong=(date('Y')-7).'-09-01';
		if ($geboortedatum>$jaaroud and $geboortedatum<=$jaarjong or $geboortedatum=='0000-00-00' or empty($geboortedatum) )
		{
			$cleanid1='';
			$cleanid2='';
			$pc1='';
			$pc2='';
			// get postal code:
			$this->query->statement('select postcode,lower(land) as land from adres where uid="'.$id_adres.'"');
			$result=$this->query->execute(true);
			$adr=$result[0];
			$pc=$adr['postcode'];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'AddtoCleanteam pc: '.$pc."; land:".$adr['land']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');

			if (empty($adr['land']) or $adr['land']=='nederland')
			{
			//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'postcode nieuw persoon: '.$pc."\r\n",3,'churchpersreg_pi3.txt');
	
				// search corresponding cleanteam:
				$this->query->statement("select t.uid,a.postcode from taakbekleding tb, taak t,persoon p ,adres a where ".
				"AES_DECRYPT(p.id_adres,@password)=a.uid and a.deleted=0 and p.deleted=0 ".
				"and tb.id_parent=t.uid and tb.id_parent=t.uid and t.omschrijving like 'cleanteam%' and p.uid=tb.id_persoon and ".
				"a.postcode >='".$pc."' and p.uid<>".$person_id." order by postcode asc");
				$result=$this->query->execute(true);
				$row1=$result[0];
			
				if (!empty($row1)) {$cleanid1=$row1['uid'];$pc1=$row1['postcode'];}
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'AddtoCleanteam gevonden cleanteam1 id1: '.$cleanid1."; pc1:".$pc1."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				
				$this->query->statement("select t.uid,a.postcode from taakbekleding tb, taak t,persoon p ,adres a where ".
				"AES_DECRYPT(p.id_adres,@password)=a.uid and a.deleted=0 and p.deleted=0 ".
				"and tb.id_parent=t.uid and tb.id_parent=t.uid and t.omschrijving like 'cleanteam%' and p.uid=tb.id_persoon and ".
				"a.postcode <='".$pc."' and p.uid<>".$person_id." order by postcode desc");
				$result=$this->query->execute(true);
				$row2=$result[0];

				if (!empty($row2)) {$cleanid2=$row2['uid'];$pc2=$row2['postcode'];}
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'AddtoCleanteam gevonden cleanteam2 id2: '.$cleanid2."; pc2:".$pc2."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				if (empty($pc2)) 
				{
					if (!empty($pc1)) $cleanid=$cleanid1; 
					else return false;
				}
				else
				{	if (empty($pc1)) $cleanid=$cleanid2;
					else
					{
						if ($cleanid1==$cleanid2) $cleanid=$cleanid1;
						else
						{
							// determine which postalcode nearest person's postal code:
							$npc=intval(substr($pc,0,4))*676 +(ord(substr($pc,5,1))-64)*26 +ord(substr($pc,6,1))-64;
							$npc1=intval(substr($pc1,0,4))*676 +(ord(substr($pc1,5,1))-64)*26 +ord(substr($pc1,6,1))-64;
							$npc2=intval(substr($pc2,0,4))*676 +(ord(substr($pc2,5,1))-64)*26 +ord(substr($pc2,6,1))-64;
							if (($npc - $npc2)<($npc1-$npc))$cleanid=$cleanid2; else $cleanid=$cleanid1;
					//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'cleanteam nieuw persoon: pc='.$pc.'('.$npc.'); pc1='.$pc1.'('.$npc1
					//	.'); pc2='.$pc2.'('.$npc2.'); cleanid1='.$cleanid1.'; cleanid2='.$cleanid2.'; cleanid='.$cleanid
					//	."\r\n",3,'churchpersreg_pi3.txt');
						}
					}
					
				}
			//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'levenshtein nieuw persoon: '.$lev1.'-'.$lev2."\r\n",3,'churchpersreg_pi3.txt');
				// add person to found cleanteam:
				$statement='insert into taakbekleding (id_parent,id_persoon,datum_start,opmerkingen) values ("'.$cleanid.'","'.$person_id.'","'.date('Y-m-d').'","")';
				$result=$this->db->query($statement);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'AddtoCleanteam '.$statement.", error:".$this->db->error."; uid:".$this->db->insert_id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			}
		}
		return true;

	}
	/**
	 * Method 'SaveAddress' for the 'churchpersreg' extension.
 	*
	 * param person: uid of selected address
	 * save address 
	 */
	function SaveAddress(&$person,$userid)
	{
		$this->ErrMsg="";
		$id_adresorg=intval($person["id_adres"]);
		$id_adres=$id_adresorg;
		$person_id=$person["person_uid"];
		$action='';
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SaveAddress id_adresorg: '.$id_adresorg.'; moveall: '.$person["moveall"]."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		
		if (!empty($id_adresorg))  // bestaand adres?
		{
			// adres gewijzigd?
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SaveAddress adres gewijzigd persoon: '.urldecode(http_build_query($person,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			if ($person["postcode"]!=$person["postcodeorg"] || $person["huisnummer"]!=$person["huisnummerorg"] || $person["straatnaam"]!=$person["straatnaamorg"] || $person["woonplaats"]!=$person["woonplaatsorg"] ) 
				if ($person["moveall"]==1) $action="change existing address";
				else $action="add new address";
			else $action="change existing address";  //|| $person["telefoonnr_vast"]!=$person["telefoonorg"]
		}
		else
		{
			//new person:
			$action="add new address";
		}
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SaveAddress action: '.$action."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		$person["postcode"]=$this->db->escape_string($person["postcode"]);
		$person["straatnaam"]=$this->db->escape_string($person["straatnaam"]);
		$person["woonplaats"]=$this->db->escape_string($person["woonplaats"]);
		$person["huisnummer"]=$this->db->escape_string($person["huisnummer"]);
		$person["land"]=$this->db->escape_string($person["land"]);
		// look for existing address:
		$statement='select * from adres where deleted=0 and postcode="'.$person["postcode"].'" and huisnummer="'.$person["huisnummer"].'" and straatnaam="'.$person["straatnaam"].'" and woonplaats="'.$person["woonplaats"].'"';
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek naar bestaande adres: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		$this->query->statement($statement);
		$result=$this->query->execute(true);
		if (!empty($result))
		{	
			$adr=$result[0];
			if (is_array($adr))
			{
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek naar bestaande adres gevonden:'.urldecode(http_build_query($adr,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				$id_adres=$adr["uid"];
			}
/*			else 
			{
				error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'zoek naar bestaande adres NIET gevonden:'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			}*/
		}
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "." action: ".$action.'; id_adres:: '.$id_adres."; id_adresorg: ".$id_adresorg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');		if ($id_adres==$id_adresorg)
		$fields='set postcode="'.$this->db->escape_string($person["postcode"]).'", huisnummer="'.$this->db->escape_string($person["huisnummer"]).'", straatnaam="'.$this->db->escape_string($person["straatnaam"]).'"'.
		', woonplaats="'.$this->db->escape_string($person["woonplaats"]).'", land="'.$this->db->escape_string($person["land"]).'"'.
		(($person['telefoonnr_vast']==$person['telefoonorg'])?'':', telefoonnr_vast=AES_ENCRYPT("'.$person['telefoonnr_vast'].'",@password)').',tel_op_website="'.
		((empty($person['tel_op_website']) or $person['tel_op_website']!="telefoon tonen")?"telefoon niet tonen":"telefoon tonen").'"'.
		',adres_op_website="'.((empty($person['adres_op_website']) or $person['adres_op_website']!="adres tonen")?"adres niet tonen":'adres tonen').'"'.	
		',steller="'.$userid.'"';
		if ($action=="change existing address")
		{
			if ($id_adres!=$id_adresorg)
			{
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "." nieuw adres bestaat al, voeg deze personen dus toe "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');		if ($id_adres==$id_adresorg)
				//nieuw adres bestaat al, voeg deze personen dus toe:
				$statement='update persoon set id_adres=AES_ENCRYPT("'.$id_adres.'",@password) where AES_DECRYPT(id_adres,@password)="'.$id_adresorg.'"';
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'voeg allen toe aan bestaande adres: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				$results=$this->db->query($statement);
				$person["id_adres"]=$id_adres; 
				// verwijder niet meer gebruikte adres:
				$results=$this->db->query('update adres set deleted=1 where uid="'.$id_adresorg.'"');
			}
			else
			{
				// Wijzig huidig adres naar nieuw:
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "." Wijzig huidig adres naar nieuw "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			if ($id_adres==$id_adresorg)
				$statement='update adres '.$fields.' where uid="'.$id_adres.'"';

				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'wijzig huidig adres5: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				$results=$this->db->query($statement);
			}
		}
		if ($action=="add new address")
		{
			if ($id_adres!=$id_adresorg)
			{
				//nieuw adres bestaat al, voeg deze persoon dus toe:
				$statement='update persoon set id_adres=AES_ENCRYPT("'.$id_adres.'",@password) where uid="'.$person_id.'"';
				if ($this->db->query($statement))$person["id_adres"]=$id_adres; 
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'voeg deze person toe aan bestaande adres: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
//				else error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'voeg deze person toe aan bestaande adres mislukt: '.$statement.'; Error:'.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');				}
			}
			else
			{
				// maak nieuw adres aan:
				$statement='insert into adres '.$fields;
/*				$statement='insert into adres set postcode="'.$person["postcode"].'", huisnummer="'.$person["huisnummer"].'", straatnaam="'.$person["straatnaam"].'", woonplaats="'.$person["woonplaats"].'", land="'.$person["land"].'"'.
				', telefoonnr_vast=AES_ENCRYPT("'.$person['telefoonnr_vast'].'",@password),tel_op_adreslijst="telefoon op adreslijst"'.
				',tel_op_website="'.((!empty($person['tel_op_website']) and $person['tel_op_website']=="telefoon tonen"?"telefoon tonen":"telefoon niet tonen").'"'.
				',adres_op_website="'.((!empty($person['adres_op_website']) and $person['adres_op_website']=="adres tonen"?"adres tonen":"adres niet tonen").'"'.	
				',steller="'.$userid.'"'; */
	
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'voeg nieuw adres toe: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
				if ($this->db->query($statement))
				{
					$id_adres=$this->db->insert_id;
					$person["id_adres"]=$id_adres; 
				}
//				else error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'toevoegen adres adres mislukt: '.$statement.'; Error:'.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
			}
		}
		return $this->ErrMsg;
		
	}
	
	/**
	 * Method 'SaveRequest' for the 'churchpersreg' extension.
 	*
	 * param uidpersoon of selected person
	 * typerequest 
	 */
	function SaveRequest($uidpersoon,$typerequest,$uidpartner)
	{
		$this->ErrMsg="";
			// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		$statement="insert ignore into aanvraag (uidpersoon,typerequest".(empty($uidpartner)?'':",uidpartner").") values('".$uidpersoon."','".$typerequest."'".(empty($uidpartner)?'':",'".$uidpartner."'").")";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SaveRequest statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Domain/Repository/debug.txt');
		$this->db->query($statement);
	}


	function AddRoepnaamEmail(&$aRoepnamen,&$aEmail,$aPers,$hoofdnaam,$type)
	{	
		if (!empty($aPers["roepnaam"]))
		{	if ($type=="adressenlijst" && strpos($aPers["achternaam"],$hoofdnaam)===false)
				array_push($aRoepnamen,$aPers["roepnaam"]." ".((empty($aPers["tussenvoegsel"]))?"":$aPers["tussenvoegsel"]." ").$aPers["achternaam"]);
			else
				array_push($aRoepnamen,$aPers["roepnaam"]);
		}
		if (!empty($aPers["emailadres"]))
		{	
			if (array_search($aPers["emailadres"],$aEmail)!==false){}
			else array_push($aEmail,$aPers["emailadres"]);
		}
	}
	function SamenstellenRegel($aPers,$aRoepnamen,$aEmail,$type)
	// samenstellen exportregel voor adressenlijst en uitvoer daarvan
	{	// samenstellen volledige naam:
		if ($type=="ledenlijst")$naam="";else $naam=$aPers["achternaam"];
	
		$l=count($aRoepnamen);
		if ($l>0)
		{	$voornaam=$aRoepnamen[0];
			for ($i=0;$i<($l-1) or ($l==1 and $i<$l);++$i)$naam.=", ".$aRoepnamen[$i];
			if ($l>1)$naam.=" en ".$aRoepnamen[$i];
		}
		//else {if (!empty($aPers["titel"]))$naam.=", ".$aPers["titel"];}
		
		if ($type=="ledenlijst"){$naam=substr($naam,2);}	
		if (!empty($aPers["tussenvoegsel"])){$naam.=" ".$aPers["tussenvoegsel"];}
		if ($type=="ledenlijst"){$naam=$naam." ".$aPers["achternaam"];$sortnaam=$aPers["achternaam"];}else $sortnaam=$naam;
	
		// bepalen telefoonnummer:
		$telefoon="";
		if (!empty($aPers["telefoonnr_vast"])) $telefoon=$aPers["telefoonnr_vast"];
		//samenstellen regel:
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Getlist samenstellerregel aExport : '.is_array($this->aExport)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		array_push($this->aExport,array($sortnaam,$naam,$aPers["straatnaam"]." ".$aPers["huisnummer"],$aPers["postcode"],$aPers["woonplaats"],$aPers["land"],$telefoon,implode(",",$aEmail)));
	}
	
	function SamenstellenGroep($aGroep,$type)
	// samenstellen groep voor adressenlijst 
	{	
		$aEmail=array();
		$aRoepnamen=array();
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Getlis SamenstellenGroep aExport : '.$this->aExport."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
	
		if (($end=count($aGroep))==1)
		{	
			$this->AddRoepnaamEmail($aRoepnamen,$aEmail,$aGroep[0],$aGroep[0]['achternaam'],$type);
			$this->SamenstellenRegel($aGroep[0],$aRoepnamen,$aEmail,$type);
		}
		else
		{
			// analyse groep:
			//	1.	Neem de eerste de beste, ga na of deze de partner of de vader/moeder van de rest is
			//		Zo ja: stop deze in aparte groep onder naam van eerste..
			//		Zo nee, ga door met volgende persoon.
			//	2.	Stop restant zelfde achternaam ook in deze groep
			//	3.	Bepaal daarvoor de adresregel
			//	4.	Doe zelfde met restant.
	
			for ($i=0;$i <$end; $i++)
			{	//	1.	Zoek iemand als partner en/of ouder:
				$id=$aGroep[$i]['id'];
				$achternaam=$aGroep[$i]['achternaam'];
				$hoofd=$aGroep[$i];
				$first=true;
				for ($j=0;$j <$end; $j++)
				{	//	1.	Zoek partner en/of kind:
					if ($aGroep[$j]['id']<>$id)
					{	if ($aGroep[$j]['id_partner']==$id || $aGroep[$j]['id_vader']==$id || $aGroep[$j]['id_moeder']==$id)
						$this->AddFirstname($first,$i,$j,$end,$aGroep,$aRoepnamen,$aEmail,$hoofd,$achternaam,$type);
					}
				}
				if ($end>0)
				{	//	2.	Stop restant zelfde achternaam ook in deze groep
					for ($j=0;$j <$end; $j++)
					{	//	1.	Zoek zelfde achternaam:
						if ($aGroep[$j]['id']<>$id)
						{	
							if ($aGroep[$j]['achternaam']==$achternaam)
								$this->AddFirstname($first,$i,$j,$end,$aGroep,$aRoepnamen,$aEmail,$hoofd,$achternaam,$type);
						}
					} 
				}
				if (!$first)
				{	//	3.	Bepaal voor selectie de adresregel
					$this->SamenstellenRegel($hoofd,$aRoepnamen,$aEmail,$type);
					break;
				}
			}
			if ($first)
			{	// niemand gevonden? ieder op aparte regel:
				for ($i=0;$i <$end; $i++)
				{	$aRoepnamen=array();
					$aEmail=array();
					$this->AddRoepnaamEmail($aRoepnamen,$aEmail,$aGroep[$i],$aGroep[$i]['achternaam'],$type);
					$this->SamenstellenRegel($aGroep[$i],$aRoepnamen,$aEmail,$type);
				}			
			}
			else
			{//	4.	Doe zelfde met restant.
				if ($end>0){$this->SamenstellenGroep($aGroep,$type);}
			}
		} 
	}
	
	function AddFirstname(&$first,&$i,&$j,&$end,&$aGroep,&$aRoepnamen,&$aEmail,$hoofd,$achternaam,$type)
	{
		if ($first)
		{	// eerst persoon zelf eraan toevoegen:
			$first=false;
			$this->AddRoepnaamEmail($aRoepnamen,$aEmail,$hoofd,$achternaam,$type);
			array_splice($aGroep,$i,1);
			if ($j>$i)$j--;
			$i--;
			$end--;
		}
		// gevonden partner/kind toevoegen:
		$this->AddRoepnaamEmail($aRoepnamen,$aEmail,$aGroep[$j],$achternaam,$type);
		array_splice($aGroep,$j,1);
		if ($i>$j)$i--;
		$end--;
		$j--;
	}

	function cmp ($a, $b) { 
	   return strcasecmp ($a[0], $b[0]); 
	} 


	
}