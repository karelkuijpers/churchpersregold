<?php
namespace Parousia\Churchpersreg\Domain\Model;

/***
 *
 * This file is part of the "Sermons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * A speaker is a person who does the preaching
 */
class Persoon extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * tstamp
     *
     * @var \DateTime
     */
    protected $tstamp = null;

    /**
     * roepnaam
     *
     * @var string
     */
    protected $roepnaam = '';

     /**
     * roepnaam
     *
     * @var string
     */
    protected $voornamen = '';

    /**
     * titel
     *
     * @var string
     */
    protected $titel = '';

   /**
     * achternaam
     *
     * @var string
     */
    protected $achternaam = '';

    /**
     * tussenvoegsel
     *
     * @var string
     */
    protected $tussenvoegsel = '';


    /**
     * naam
     *
     * @var string
     */
    protected $naam = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * straatnaam
     *
     * @var string
     */
    protected $straatnaam = '';

    /**
     * huisnummer
     *
     * @var string
     */

    protected $huisnummer = '';

    /**
     * postcode
     *
     * @var string
     */

    protected $postcode = '';

    /**
     * woonplaats
     *
     * @var string
     */

    protected $woonplaats = '';

    /**
     * deleted
     *
     * @var bool
     */

    protected $deleted = false;

    /**
     * hidden
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * starttime
     *
     * @var \DateTime
     */
    protected $starttime = null;

    /**
     * endtime
     *
     * @var \DateTime
     */
    protected $endtime = null;

    /**
     * foto
     *
     * @var string
     */
    protected $foto = null;

    /**
     * Returns the roepnaam
     *
     * @return string $roepnaam
     */


    public function getRoepnaam()
    {
        return $this->roepnaam;
    }
    /**
     * Sets the roepnaam
     *
     * @param string $roepnaam
     * @return void
     */
    public function setRoepnaam($roepnaam)
    {
        $this->roepnaam = $roepnaam;
    }

	 /**
     * Returns the voornamen
     *
     * @return string $voornamen
     */
    public function getVoornamen()
    {
        return $this->voornamen;
    }
    /**
     * Sets the voornamen
     *
     * @param string $voornamen
     * @return void
     */
    public function setVoornamen($voornamen)
    {
        $this->voornamen = $voornamen;
    }

	 /**
     * Returns the titel
     *
     * @return string $titel
     */
    public function getTitel()
    {
        return $this->titel;
    }
    /**
     * Sets the titel
     *
     * @param string $titel
     * @return void
     */
    public function setTitel($titel)
    {
        $this->titel = $titel;
    }

	 /**
     * Returns the achternaam
     *
     * @return string $achternaam
     */
    public function getAchternaam()
    {
        return $this->achternaam;
    }

    /**
     * Sets the achternaam
     *
     * @param string $achternaam
     * @return void
     */
    public function setAchternaam($achternaam)
    {
        $this->achternaam = $achternaam;
    }

    /**
     * Returns the naam
     *
     * @return string $naam
     */
    public function getNaam()
    {
        return $this->naam;
    }
    /**
     * Sets the naam
     *
     * @param string $naam
     * @return void
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    /**
     * Returns the tussenvoegsel
     *
     * @return string $tussenvoegsel
     */
    public function getTussenvoegsel()
    {
        return $this->tussenvoegsel;
    }

    /**
     * Sets the tussenvoegsel
     *
     * @param string $tussenvoegsel
     * @return void
     */
    public function setTussenvoegsel($tussenvoegsel)
    {
        $this->tussenvoegsel = $tussenvoegsel;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the straatnaam
     *
     * @return string $straatnaam
     */
    public function getStraatnaam()
    {
        return $this->straatnaam;
    }

    /**
     * Sets the straatnaam
     *
     * @param string $straatnaam
     * @return void
     */
    public function setStraatnaam($straatnaam)
    {
        $this->straatnaam = $straatnaam;
    }

    /**
     * Returns the huisnummer
     *
     * @return string $huisnummer
     */
    public function getHuisnummer()
    {
        return $this->huisnummer;
    }

    /**
     * Sets the huisnummer
     *
     * @param string $huisnummer
     * @return void
     */
    public function setHuisnummer($huisnummer)
    {
        $this->huisnummer = $huisnummer;
    }

    /**
     * Returns the postcode
     *
     * @return string $postcode
     */
    public function getpostcode()
    {
        return $this->postcode;
    }

    /**
     * Sets the postcode
     *
     * @param string $postcode
     * @return void
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * Returns the woonplaats
     *
     * @return string $woonplaats
     */
    public function getWoonplaats()
    {
        return $this->woonplaats;
    }

    /**
     * Sets the woonplaats
     *
     * @param string $woonplaats
     * @return void
     */
    public function setWoonplaats($woonplaats)
    {
        $this->woonplaats = $woonplaats;
    }

    /**
     * Returns the foto
     *
     * @return mediumblob foto
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Sets the Foto
     *
     * @param mediumblok $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    /**
     * Returns the deleted
     *
     * @return bool $deleted
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param bool $deleted
     * @return void
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Returns the boolean state of deleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Returns the tstamp
     *
     * @return \DateTime $tstamp
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Sets the tstamp
     *
     * @param \DateTime $tstamp
     * @return void
     */
    public function setTstamp(\DateTime $tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the hidden
     *
     * @return bool $hidden
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Sets the hidden
     *
     * @param bool $hidden
     * @return void
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns the boolean state of hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * Returns the starttime
     *
     * @return \DateTime $starttime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Sets the starttime
     *
     * @param \DateTime $starttime
     * @return void
     */
    public function setStarttime(\DateTime $starttime)
    {
        $this->starttime = $starttime;
    }

    /**
     * Returns the endtime
     *
     * @return \DateTime $endtime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Sets the endtime
     *
     * @param \DateTime $endtime
     * @return void
     */
    public function setEndtime(\DateTime $endtime)
    {
        $this->endtime = $endtime;
    }
}
