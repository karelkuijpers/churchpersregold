<?php
namespace Parousia\Churchpersreg\Controller;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use Parousia\Churchpersreg\Hooks\FluidEmailReal;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\RawMessage;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;


/***
 *
 * This file is part of the "Persoons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * PersoonController
 */
class PersoonController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \Parousia\Churchpersreg\Domain\Repository\PersoonRepository
     */
    protected $PersoonRepository;
	public static $includedMyJs;
	protected $aExport=array();
	protected $args=array();
	protected $downloadfile='';
	protected $search= array();
	protected $return='';
	protected $personid='';
	protected $pageId;
	protected $myprofile=0;
	protected $stack=array();
	protected $extensionConfiguration;
	protected $aRelationship=
		[ 
			'broederzuster' => 
			[
				'man' => 'broeder',
				'vrouw' => 'zuster'
			],
			'heermevr' =>
			[
				'man' => 'heer',
				'vrouw' => 'mevrouw'				
			]
		];
	var $userid;
	var $username='';
	var $frontendUser;

    /**
     * Inject a persoon repository to enable DI
     *
     * @param \Parousia\Churchpersreg\Domain\Repository\PersoonRepository $persoonRepository
     */
    public function injectPersoonRepository(\Parousia\Churchpersreg\Domain\Repository\PersoonRepository $persoonRepository)
    {
        $this->PersoonRepository = $persoonRepository;
    }

    /**
     * Initialize redirects
     */
    public function initializeAction(): void
    {
		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Initialize args '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (isset($this->args['stack']))$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Initialize stack '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (isset($this->args['downloadfile']))$this->downloadfile=$this->args['downloadfile'];
		if (isset($this->args['search']))$this->search=$this->args['search'];
		if(isset($this->args['return']))$this->return=$this->args['return'];
		if(isset($this->args['personid']))$this->personid=$this->args['personid'];
		if (!empty($this->args['myprofile']))$this->myprofile=1;
		elseif (empty($this->personid) && empty($this->stack))$this->myprofile=1;
//		if(isset($this->args['person_uid']))$this->personid=$this->args['person_uid'];
		$pageArguments = $this->request->getAttribute('routing');
		$this->pageId = $pageArguments->getPageId();
		$this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
		$this->frontendUser = $this->request->getAttribute('frontend.user');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Initialize args frontend->user: '.urldecode(http_build_query($this->frontendUser->user,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->userid=$this->frontendUser->user['person_id'];
		$this->username=$this->frontendUser->user['name'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction personid: '.$this->personid.', userid: '.$this->userid.', user: '.$this->username.', return: '.$this->return.'; args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
	}
	
    /**
     * action searchForm
     *
     * @return void
     */
    public function searchFormAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Smoelenboek lezen"))die("You don't have the privilege to perform this action");
//		$this->args=$this->request->getArguments();
		if (!empty($this->stack))
		{
			$key=array_search('selectedList', array_column($this->stack, 'action'));
			if ($key)$this->search=$this->stack[$key]["search"];
		}
		
        if (empty($this->search)) 
		{
            $this->search = array();
			$this->search['bezoeker']=1;
			$this->search['general']="";
			$this->search['inclverwijderd']="0";
        }
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SearchForm search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		// reinitialize stack:
		$this->stack=array(array("action"=>"searchForm","search"=>$this->search));
//var_dump($this->search);
    	$assignedValues = [
			'search' => $this->search,
			'downloadfile' => $this->downloadfile,
			'stack' => json_encode($this->stack),
			'personreg' => (int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"),
			'exportpersonen' => (int)churchpersreg_div::Heeftpermissie("Export personen"),
			'emailpersonen' => (int)churchpersreg_div::Heeftpermissie("Email personen"),
			'schrijfpersonen' => (int)churchpersreg_div::Heeftpermissie("Persoon schrijven"),
			'removedread' => (int)churchpersreg_div::Heeftpermissie("Verwijderde lezen"),
			'extPath' => PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg')),
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

    /**
     * action selectedList
     *
     * @return void
     */
    public function selectedListAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Smoelenboek lezen"))die("You don't have the privilege to perform this action");
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search : '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (empty($this->stack))$this->stack=[array('action'=>"searchForm","search"=>$this->search),array("action"=>"selectedList","search"=>$this->search)];
		else
		{
			$key=array_search('selectedList', array_column($this->stack, 'action'));
			if (!$key)$this->stack[]=array("action"=>"selectedList","search"=>$this->search);
			else 
			{
				$this->search=$this->stack[$key]['search'];
				array_splice($this->stack,$key+1);  // reset stack from this action off
			}
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (empty($this->search['general']) && empty($this->search['voornaam']) && empty($this->search['achternaam'])&&empty($this->search['postcode'])&&empty($this->search['woonplaats'])&&empty($this->search['leeftijdmin'])&&empty($this->search['leeftijdmax'])&&empty($this->search['leeftijdmin'])
		&&empty($this->search['inputdatmax'])&&empty($this->search['inputdatmin'])&&empty($this->search['liddatmax'])&&empty($this->search['liddatmin'])&&empty($this->search['wijzdatmin'])&&empty($this->search['wijzdatmax']))
		{
	        return (new ForwardResponse('searchForm'));	
		}
		else $persons=$this->PersoonRepository->findSelection($this->search);
		//$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
		if (!empty($this->search['peradres']))
		{
			// expand huisgenoten to an array:
			for ($i=0;$i<count($persons);$i++)
			{
				$huisgenot=explode(', ',$persons[$i]['huisgenoten']);
				$huisgenoten=array();
				for ($j=0;$j<count($huisgenot);$j++) 
				{
					$huisg=explode('#',$huisgenot[$j]);
					$huisgenoten[]=['person_uid'=> $huisg[0],'naam'=>$huisg[1],'deleted'=>$huisg[2]];
				}
				$persons[$i]['huisgenoten']=$huisgenoten;
			}
		}
		$peradres='0';
		if (!empty($this->search['peradres']))$peradres=$this->search['peradres'];
    	$assignedValues = [
			'persons' => $persons,
			'stack' => json_encode($this->stack),
			'downloadfile' => $this->downloadfile,
            'currentPageId' => $this->pageId,
			'userid' => $this->userid,
			'emailchangeaddresses' => $this->extensionConfiguration['emailchangeaddresses'],
			'personreg' => (int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"),
			'adressenlijstreg' => (int)churchpersreg_div::Heeftpermissie("Export adressenlijst"),
			'exportpersonen' => (int)churchpersreg_div::Heeftpermissie("Export personen"),
			'emailpersonen' => (int)churchpersreg_div::Heeftpermissie("Email personen"),
			'schrijfpersonen' => (int)churchpersreg_div::Heeftpermissie("Persoon schrijven"),
			'peradres' => $peradres,
        ];
        $this->view->assignMultiple($assignedValues); 

		return $this->htmlResponse();
    }

    /**
     * action email
     *
     * @return void
     */
    public function emailAction(): ResponseInterface
    {
		// this follows detailaction with person_uid or selectedList with search parameters
		if (!(int)churchpersreg_div::Heeftpermissie("Email personen"))die("You don't have the privilege to perform this action");
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'email args : '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'email stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//$key=array_search('selectedList', array_column($this->stack, 'action'));
		$key=count($this->stack)-1;
		if (isset($this->stack[$key]['action']) && $this->stack[$key]['action']=='detail')
		{
			$this->stack[]=array("action"=>"email","personid"=>$this->personid);
	        $person = $this->PersoonRepository->findByUid($this->personid);
			$persons[]=$person;
			$this->return='detail';
		}
		else
		{
			if (isset($this->stack[$key]['search']))$this->search=$this->stack[$key]['search'];
			$this->search['emailadres']='@';
			$this->stack[]=array("action"=>"email","search"=>$this->search);
			$persons=$this->PersoonRepository->findSelection($this->search);
			$this->return='selectedList';
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Email stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$recipients = array();
		$aPersonArchive=array();
		$aantalgevonden=count($persons);
		//$BccStr='';

		foreach ($persons as $person)
		{	
			//if (array_key_exists($person['emailadres'],$recipients))
			//{	$recipients[$person['emailadres']].=" en ".$person['naam'];}
			//else 
//				$recipients[$person['emailadres']]=$person['naam'];
			$recipients[$person['naam']]=$person['emailadres'];
			$aPersonArchive[]=
			[
				'uid'=>$person['person_uid'],
				'lastname'=>$person['lastname'],
				'roepnaam'=>$person['roepnaam'],
				'emailadres'=>$person['emailadres'],
				'geslacht'=>$person['geslacht']
			];
		}
//		$BccStr="";
//		foreach ($recipients as $key => $value)$BccStr.=$key.'<'.$value.'>; ';
		//foreach ($recipients as $key => $value)$BccStr.=$value.'<'.$key.'>; ';
//		$BccStr=substr($BccStr,0,strlen($BccStr)-2);
		//$PersonArchiveStr=implode(",",$aPersonArchive);
		$appreciation=explode(',',$this->extensionConfiguration['appreciation']);
		foreach ($appreciation as &$appreciatn) $appreciatn=['key'=>$appreciatn,'label'=>$appreciatn];
		$aRelationship=explode(',',$this->extensionConfiguration['relationship']);
		foreach ($aRelationship as &$relation) $relation=explode('&',$relation);
		$relationships=array_column($aRelationship,0);
		foreach ($relationships as &$relation) $relation=['key'=>$relation,'label'=>$relation];
	//	$aRealtionships= array();
//		foreach ($aRelationship as $relation)$aRealtionships[$relation[0]]=['man'=>$relation[1],'vrouw'=>$relation[2]];

    	$assignedValues = [
		//	'BccStr' => $BccStr,
			'persons' => $persons,
			'aPersonArchive' => json_encode($aPersonArchive),
			'aantalgevonden' => $aantalgevonden,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'username' => $this->username,
			'useremail' =>$this->frontendUser->user['email'],
			'contactarchiefreg' => (int)churchpersreg_div::Heeftpermissie("ContactArchief schrijven"),
			'extPath' => PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg')),
			'return' => $this->return,
			'appreciations'=> $appreciation,
			'appreciationdefault'=>$this->extensionConfiguration['appreciationdefault'],
			'relationships' => $relationships,
	//		'aRelationship' => $aRelationship,
	//		'aRealtionships'=> $aRealtionships,
			'relationshipdefault'=> $this->extensionConfiguration['relationshipdefault'],
			'namedefault'=> $this->extensionConfiguration['namedefault'],
        ];
        $this->view->assignMultiple($assignedValues); 

		$assetCollector = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\AssetCollector::class);
		//$assetCollector->addJavaScript('Searchpersonrole', 'EXT:churchtakenreg/Resources/Public/Javascript/Searchpersonrole.js',[], ['priority' => true]);
		$assetCollector->addJavaScript('ckeditor', "https://cdn.ckeditor.com/ckeditor5/33.0.0/classic/ckeditor.js",[], ['priority' => true]);
//		$assetCollector->addJavaScript('nl', 'EXT:parousiazoetermeer/Resources/Public/Javascript/nl.js',[], ['priority' => true]);
		return $this->htmlResponse();
    }
    /**
     * action sendEmail
     *
     * @return void
     */
    public function sendEmailAction(): ResponseInterface
    {
		// this follows detailaction with person_uid or selectedList with search parameters
		if (!(int)churchpersreg_div::Heeftpermissie("Email personen"))die("You don't have the privilege to perform this action");
		$error='';
		if (isset($this->args['recipients']))
		{
			$Recipients=preg_replace("/[;,]+[\s;]*/",',', trim($this->args['recipients']));
		}
		$persons=$this->PersoonRepository->findRecipients($Recipients);
		$aantalgevonden=count($persons);

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EmailPersons persons('.$aantalgevonden.'): '.urldecode(http_build_query($persons,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt");
		//$aantalgevonden=0;
		if ($aantalgevonden==0)
			$error="Geen geadresserden gevonden";
		else
		{
			$archive=$this->args['archive'];
			$confidential=$this->args['confidential'];
			$bodycontent = html_entity_decode(urldecode($this->args['message']));
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EmailPersons aRelationship('.GeneralUtility::array2xml($this->aRelationship)."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt");
			
			if (Empty($this->args['username']))
				$sendername=$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'];
			else
				$sendername=$this->args['username'].' intern';

			$this->PersoonRepository->selectTransport($aantalgevonden);
			// Create the message
			$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
	
			// Prepare and send the message
			$mail
			   // Give the message a subject
			   ->subject($this->args['subject'])
			   // Set the From address with an associative array
			   ->from(new Address($GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'],$sendername))
			   // Give it a body
			;
			if (!Empty($this->args['useremail']) && stripos($this->args['useremail'],'xs4all') === false)
			{	
				$mail->replyTo(new Address($this->args['useremail'],$sendername));
			//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EmailPersons getreplyto: '.$mail->getReplyTo()."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3temp/churchadmin/debug/debug.txt");
			}
			// remove old emails:
			$spoolpath=$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_spool_filepath'];
			if (!file_exists($spoolpath))mkdir($spoolpath, 0770,true);
			// remove old spool files:
			$fileSystemIterator = new \FilesystemIterator($spoolpath);
			$expire = strtotime('-3 DAYS');
			foreach ($fileSystemIterator as $file) {
			    if ($file->getCTime() < $expire) 
			        unlink($spoolpath.'/'.$file->getFilename());
			}
			// make upload dir:
			$extpath=$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/spoolfiles/';
			if (!file_exists($extpath))mkdir($extpath, 0777,true);
			// remove old files belonging to emails:
			$fileSystemIterator = new \FilesystemIterator($extpath);
			$expire = strtotime('-3 DAYS');
			foreach ($fileSystemIterator as $file) {
			    if ($file->getCTime() < $expire) 
			        unlink($extpath.$file->getFilename());
			}

			if (!empty($_FILES)){
			
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EmailPersons files: '.GeneralUtility::array2xml($_FILES)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
				foreach($_FILES as $key => $FILE)
				{	
					for ($i=0;$i<count($FILE["name"]);$i++)
					{
						if (!empty($FILE["tmp_name"][$i]) and !empty($FILE["name"][$i]))
						{	
							$filetmp=$FILE["tmp_name"][$i];
							$filedst=$extpath.basename($filetmp);
							if (move_uploaded_file ($filetmp,$filedst))
							{
								$cnt++;
								chmod($filedst,0770);
							   // Optionally add any attachments
								$mail->attachFromPath($filedst,$FILE["name"][$i]);
								if ($archive)
								{
									//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": added archive file: ".urldecode(http_build_query([$filedst,$FILE["name"][$i]],NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
									$afilepath[]=[$filedst,$FILE["name"][$i]];
									//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].":afilepath after added archive file: ".urldecode(http_build_query($afilepath,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');
								}
							}
						}
					}
				}
			}
			$nbrsend=0;
			// Compose salutation:
			if (empty($this->args['appreciation']))$appreciation='';
			else $appreciation=strval($this->args['appreciation']);
			if (isset($this->args['relationship']))$relationship=$this->args['relationship'];else $relationship='';
			$relationships=explode(',',$this->extensionConfiguration['relationship']);
			foreach ($relationships as &$relation) $relation=explode('&',$relation);
			$aRelationship= array();
			foreach ($relationships as $relation)$aRelationship[$relation[0]]=['man'=>$relation[1],'vrouw'=>$relation[2]];
			

			if(isset($this->args['nameformat']))$nameformat=$this->args['nameformat'];else $nameformat='';
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": sendEmailAction appreciation: ".$appreciation.'; relationship: '.$relationship.'; nameformat: '.$nameformat."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt");
			foreach ($persons as $person)
			{	
				//$aBcc=explode('<',$aRecipients[$j]);
				//$aBcc=preg_split("/[\<\>]+/",$aRecipients[$j]);
				$emailadres=$person['emailadres'];
				$name=$person['naam'];
				$salutation=$appreciation;
				if (!empty($relationship))
				{
					$geslacht=$person['geslacht'];
					$salutation.=' '.$aRelationship[$relationship][$geslacht];
					$salutation=ucfirst(ltrim($salutation));
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": sendEmailAction geslacht: ".$geslacht.'; salutation: '.$salutation."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt");
				}
				if (!empty($nameformat))
				{
					$salutation.=' ';
					switch($nameformat){
						case 'firstname';
							$salutation.=$person['roepnaam'];
							break;
						case 'fullname';
							$salutation.=$person['naam'];
							break;
						case 'lastname';
							$salutation.=$person['lastname'];
							break;
					}
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": sendEmailAction nameformat: ".$nameformat.'; salutation: '.$salutation."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt");
				}
				$salutation=Trim($salutation);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": sendEmailAction salutation: ".$salutation."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt");
				
				// Set content:
				if (!empty($salutation)) $mail->html($salutation.',<br>'.$bodycontent);
				else $mail->html($bodycontent);
			    
				$mail->to(new Address($emailadres, $name));
				// And finally do send it
				$mail->send();
				$nbrsend++;
			}
			// archiving:
			
			if ($archive=='archiveren' and $nbrsend<10)
			{	// archive email at each person:
				//$aArchivepersons=explode(",",$archivepersons);
				// initialize mysqli:
				churchpersreg_div::connectdb($db);
				$today=date("Y-m-d");
				//$content=$db->real_escape_string($bodycont);			
				$querybase='insert into contact_archive (subject,author,date,id_person_owner,confidential,type_content,document_name,content,id_person,id_parentarchive) values (AES_ENCRYPT("'.$db->real_escape_string($this->args['subject']).'",@password),"'.$db->real_escape_string($this->args['username']).'"'.
				',"'.$today.'","'.$this->args['userid'].'","'.(($confidential=='vertrouwelijk')?'1':'0').'"';  
				
				foreach ($persons as $archiveperson)
				{	//skip failed recipients?
					//insert new contact_archive record:
					$parent_id=0;
					if (empty($archiveperson['uid'])) loop;
					$persid=$archiveperson['uid'];
					// attachements only when less than 4 recipients (otherwise documents on other place on website):
					$query=$querybase.',"email","",AES_ENCRYPT("'.$bodycontent.'",@password),"'.$persid.'","'.$parent_id.'")';
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": archive emailsend insert: ".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3temp/churchadmin/debug/debug.txt");
					$db->query($query);
			//		if (!$db->query($db,$query)===true or !$db->errno===0)
					if (!$db->errno===0)
					{
						$sqlerr=$db->error;
						$error .="<br>Bewaren contactarchief mislukt:$sqlerr";
						////error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": archive emailsend insert failed: ".$db->error.'; nr:'.$db->errno."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3temp/churchadmin/debug/debug.txt");
						churchpersreg_div::Trace($error,"Email.log");
					}
					//else error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": archive emailsend insert rows: ".$db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3temp/churchadmin/debug/debug.txt");
			
					$parent_id=$db->insert_id;
					if (is_array($afilepath) && $aantalgevonden<4)
					{
//						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'archive files : '.urldecode(http_build_query($afilepath,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.log');

						foreach ($afilepath as $filepath)
						{
							$fp      = fopen($filepath[0], 'r');
							$content = fread($fp, filesize($filepath[0]));
						//	churchpersreg_div::Trace("file: ".$filepath[0].": size file:".filesize($filepath)."(".$FILE['size'].") ; content len:".strlen($content),"Email.log");
							fclose($fp);
							//insert in archive as child
							$query=$querybase.',"bestand",AES_ENCRYPT("'.$filepath[1].'",@password),AES_ENCRYPT("'.$db->real_escape_string($content).'",@password),"'.$persid.'","'.$parent_id.'")';
							$db->query($query);
							if (!$db->errno===0)
							{
								$sqlerr=$db->error;
								$error .="<br>Bewaren file contactarchief mislukt:$sqlerr";
								//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": archive emailsend insert file failed: ".$db->error.'; nr:'.$db->errno."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3temp/churchadmin/debug/debug.log");
								churchpersreg_div::Trace($error,"Email.log");
							}
							//else error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": archive emailsend insert file rows: ".$db->affected_rows."; sql_error:".$sqlerr."\r\n",3,$_SERVER['DOCUMENT_ROOT']."/typo3temp/churchadmin/debug/debug.txt");
						}
					}
				}
				// remove attachement files: don't do it in case of spoll to file
				//foreach ($afilepath as $file) unlink($file[0]);
				
			} 
			
			//churchpersreg_div::Trace("Verzonden aan ".$nbrsend." van ".$aantalgevonden." geselecteerden.","debug.txt");
			if ($aantalgevonden==1 && $nbrsend>0)churchpersreg_div::Trace("Verzonden '".$this->args['subject']."' aan ".$name." (".$emailadres.")","Mail.log");
			else churchpersreg_div::Trace("Verzonden '".$this->args['subject']."' aan ".$nbrsend." van ".$aantalgevonden." geselecteerden.","Mmail.log");
			
		}
		//return $response.'<br>'.GeneralUtility::arrayToLogString( $aoptions);
		$assignedValues = [
			'stack' => $this->args['stack'],
		];
        return (new ForwardResponse($this->return))
              ->withArguments($assignedValues);
	}


    /**
     * action detail
     *
     * @return void
     */
    public function detailAction(): ResponseInterface
    { 
		if (!(int)churchpersreg_div::Heeftpermissie("Smoelenboek lezen"))die("You don't have the privilege to perform this action");
		if (isset($this->args['subaction']) and $this->args['subaction']=='save')
		{
			$Errmsg=$this->saveAction();
//			if (!empty($Errmsg))return $Errmsg;
		}
		$retextensionName='';
		$retpluginName='';
		$retpageUid='';
		$retcontroller='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction personid: '.$this->personid.'; is stack: '.!empty($this->stack)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (!empty($this->personid))
		{
			// forward:
			$ptr=count($this->stack)-1;
			$this->return=$this->stack[$ptr]["action"];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction forward return: '.$this->return.'; stack['.$ptr.']: '.urldecode(http_build_query($this->stack[$ptr],NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			if (isset($this->stack[$ptr]["extensionName"]))$retextensionName=$this->stack[$ptr]["extensionName"];
			if (isset($this->stack[$ptr]["pluginName"]))$retpluginName=$this->stack[$ptr]["pluginName"];
			if (isset($this->stack[$ptr]["pageUid"]))$retpageUid=$this->stack[$ptr]["pageUid"];
			if (isset($this->stack[$ptr]["controller"]))$retcontroller=$this->stack[$ptr]["controller"];
			$this->stack[]=array("action"=>"detail","controller"=>"Persoon","extensionName"=>"churchpersreg","pluginName"=>"Persoon","pageUid"=>$this->pageId,"personid"=>$this->personid);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction pers return:'.$this->return.', stack nw:'.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		}
		else
		{
			// return to this action:
			if (!empty($this->stack))
			{
				if(count($this->stack)>1)array_pop($this->stack); // remove latest action
				// search latest occurrence of detail:
				$key=array_search('detail', array_reverse(array_column($this->stack, 'action'),true));
				if (isset($this->stack[$key]['personid']))$this->personid=$this->stack[$key]['personid'];
				array_splice($this->stack,$key+1);  // reset stack from this action off
				//if (!$key)$this->stack[]=array("action"=>"selectedList","search"=>$this->search);
				if ($key >0)
				{ 
					$ptr=$key-1;
					if (isset($this->stack[$ptr]["action"]))$this->return=$this->stack[$ptr]["action"];
					if (isset($this->stack[$ptr]["extensionName"]))$retextensionName=$this->stack[$ptr]["extensionName"];
					if (!empty($this->stack[$ptr]["pluginName"]))$retpluginName=$this->stack[$ptr]["pluginName"];
					if (isset($this->stack[$ptr]["pageUid"]))$retpageUid=$this->stack[$ptr]["pageUid"];
					if (isset($this->stack[$ptr]["controller"]))$retcontroller=$this->stack[$ptr]["controller"];
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction return to,  return: '.$this->return.'; personid['.$this->personid.']: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
				}
/*
				$ptr=count($this->stack);
				if ($ptr>1)
				{	
					array_pop($this->stack);  // reset stack from this action off
					$ptr=$ptr-1;
				}
				if ($ptr>0)
				{
					$ptr=$ptr-1;
					if (isset($this->stack[$ptr]['personid']))$this->personid=$this->stack[$ptr]['personid'];
					
					if ($ptr>0)$ptr=$ptr-1;
					if ($ptr>0)if (isset($this->stack[$ptr]["action"]))$this->return=$this->stack[$ptr]["action"];
					if (isset($this->stack[$ptr]["extensionName"]))$retextensionName=$this->stack[$ptr]["extensionName"];
					if (!empty($this->stack[$ptr]["pluginName"]))$retpluginName=$this->stack[$ptr]["pluginName"];
					if (isset($this->stack[$ptr]["pageUid"]))$retpageUid=$this->stack[$ptr]["pageUid"];
					if (isset($this->stack[$ptr]["controller"]))$retcontroller=$this->stack[$ptr]["controller"];
					error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction return to,  return: '.$this->return.'; personid['.$this->personid.']: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
				} */
			}
			else 
			{	// mijn profiel:
				$this->personid=$this->userid;
				$this->stack[]=array("action"=>"detail","controller"=>"Persoon","extensionName"=>"churchpersreg","pluginName"=>"Persoon","pageUid"=>$this->pageId,"personid"=>$this->personid);
				//$this->stack[]=array("action"=>"detail","personid"=>$this->personid);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction mijn profiel personid: '.$this->personid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
				$this->return='';
			}
		}
/*		$key=array_search('detail', array_column($this->stack, 'action'));
		if (is_array($this->stack) && count($this->stack)>0)
			$this->return=$this->stack[count($this->stack)-1]["action"];
		else $this->return="selectedList";
		if (!$key or $this->personid)	$this->stack[]=array("action"=>"detail","personid"=>$this->personid);
		else 
		{
		} */
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction voor findByUid stack: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

	//	var_dump($this->search);
		setlocale(LC_TIME, 'NL_nl');
	//	var_dump($this->personid);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction findByUid personid: '.$this->personid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
        $person = $this->PersoonRepository->findByUid($this->personid);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction findByUid person: '.urldecode(http_build_query($person,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (!empty($person['verwijderreden']))$person['verwijderreden']=churchpersreg_div::$verwredenen[$person['verwijderreden']];else $person['verwijderreden']='';
		$personreg = (int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven");
		$contactarchief= churchpersreg_div::Heeftpermissie("ContactArchief lezen,ContactArchief schrijven");

//		$extPath=PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg'));
//		$usermap=$extPath."export/".$this->username;
		$extpath='typo3temp/churchadmin/';
		$name=str_replace(" ","",$this->username);
		if (!file_exists($extpath."export"))mkdir($extpath."export", 0777,true);
		$usermap=$extpath."export/".$this->username;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction unlink files usermap: '.$usermap."; user: ".$this->username."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		// check if map exists:
		if (!file_exists($usermap))mkdir($usermap, 0777);
		else // clear map 
			foreach (glob($usermap."/*") as $filename)unlink($filename);
		$archs=array();
		if ($personreg && $contactarchief)
		{	
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction findArchives: personid '.$this->personid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			$archives=$this->PersoonRepository->findArchives($this->personid,$this->userid);
			if ($archives)
			{
				foreach ( $archives as $archive)
				{
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction findArchives: '.$archive['id_parentarchive']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

					if (empty($archive['id_parentarchive']))
						$docname=$archive['document_name'];
					else
					{
						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction findArchives: document_name '.$archive['document_name']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
						$aDoc=explode(".",$archive['document_name']);
						$docname=$aDoc[0].$archive['uid'].'.'.$aDoc[1];
					}
					if (!empty($docname))
					{	file_put_contents($usermap.'/'.$docname, $archive['content']);
						chmod($usermap.'/'.$docname, 0777);
					}
					$arch=
					[
						'uid'=>$archive['uid'],
						'id_parentarchive'=> $archive['id_parentarchive'],
						'subject'=>$archive['subject'],
						'author'=>$archive['author'],
						'date'=>$archive['date'],
						'document_name'=>$archive['document_name'],
						'type_content'=>$archive['type_content'],
						'filename'=>$docname,
					];
					$archs[]=$arch;
				}
			}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction return:'.$this->return.', archive: '.urldecode(http_build_query($archs,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		}

		//$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');

		//$extensionConfiguration['emailchangeaddresses']
		if ($personreg==1||$person['id_vader']==$this->userid || $person['id_moeder']==$this->userid||$person['id_partner']==$this->userid|| $person['person_uid']==$this->userid)
			$ExtraPermissions=1; 
		else $ExtraPermissions=0;
		
		//exploded taken:
		$taken=array();
		if (!empty($person['taken']))
		{
			$aTaken=explode(",",$person['taken']);
	//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction aTaken: '.urldecode(http_build_query($aTaken,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			$taken=array();
			$taken=array_map(function($taak){return explode('#',$taak);},$aTaken);
			$taken= array_column($taken,1,0);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction taken erna: '.urldecode(http_build_query($taken,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		}

		//explode children:
		$children=array();
		if (!empty($person['children']))
		{
			$aChildren=explode(",",$person['children']);
			$children=array_map(function($child){return explode('#',$child);},$aChildren);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction children compleet: '.urldecode(http_build_query($children,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			//$children= array_column($children,1,0);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction children kolom 1: '.urldecode(http_build_query($children,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		}
		$oldenoughforparent=0;
		if (array_key_exists('geboortedatum',$person))
		{	
			$geboortedatum = new \DateTime($person['geboortedatum']);
			$age16= new \DateTime('- 16 years');
			if ($geboortedatum < $age16)$oldenoughforparent=1;
		}


		
//var_dump($ExtraPermissions);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction assignedValued return:'.$this->return."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction voor assignedValues stack: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		if (empty($this->return) or !empty($this->myprofile) )
		{
			//Mijn profiel:
			$assignedValues = [
			'person' => $person,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'oldenoughforparents'=> $oldenoughforparent,
			'emailchangeaddresses' => $this->extensionConfiguration['emailchangeaddresses'],
			'pagenbrministrytree' => $this->extensionConfiguration['pagenbrministrytree'],
			'taken' => $taken,
			'children' => $children,
			'extraPermissions' => $ExtraPermissions,
			'myprofile'=>$this->myprofile, 
			'userlid' => $this->frontendUser->user['lid'],
			'extPath' => $extpath,
			'return'=>$this->return,
			];
		}
		else
		{
			$assignedValues = [
				'person' => $person,
				'stack' => json_encode($this->stack),
				'archives' => $archs,
	            'currentPageId' => $this->pageId,
				'userid' => $this->userid,
				'oldenoughforparents'=> $oldenoughforparent,
				'emailchangeaddresses' => $this->extensionConfiguration['emailchangeaddresses'],
				'extraPermissions' => $ExtraPermissions,
				'myprofile'=>$this->myprofile,
				'pagenbrministrytree' => $this->extensionConfiguration['pagenbrministrytree'],
				'taken' => $taken,
				'children' => $children,
				'userlid' => $this->frontendUser->user['lid'],
				'cleanteamReg'=> churchpersreg_div::Heeftpermissie("Cleanteams samenstellen"),
				'emailpersons' => churchpersreg_div::Heeftpermissie("Email personen"),
				'schrijvenpersons' => churchpersreg_div::Heeftpermissie("Persoon schrijven"),
				'contactarchief' => $contactarchief,
				'contactarchiefschrijven' => churchpersreg_div::Heeftpermissie("ContactArchief schrijven"),
				'contactarchiefverwijderd' => churchpersreg_div::Heeftpermissie("ContactArchief lezen verwijderd"),
				'extPath' => $extpath,
				'usermap' =>$usermap,
				'personreg' => $personreg,
				'return' => $this->return,		
				'verwreden' => churchpersreg_div::$verwredenen,
			];
//			if($retpageUid!==$retpageUid)
			if(!empty($retpageUid))
			{
				$assignedValues['retcontroller'] = $retcontroller;
				$assignedValues['retextensionName'] = $retextensionName;
				$assignedValues['retpluginName'] = $retpluginName;
				$assignedValues['retpageUid'] = $retpageUid;
			}
		};
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction return:'.$this->return.', assignedValues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();

    } 

    /**
     * action edit
     *
     * @return void
     */
    public function editAction(): ResponseInterface
    {
		$personreg= (int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven");
		if (!$personreg) die("You don't have the privilege to perform this action");
//		$this->args=$this->request->getArguments();
		if (empty($this->args['typeadd']))$typeadd=''; else $typeadd=$this->args['typeadd'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'editAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->return=$this->stack[count($this->stack)-1]['action'];
		$key=array_search('edit', array_column($this->stack, 'action'));
		if (!$key)$this->stack[]=array("action"=>"edit","personid"=>$this->personid);
		else array_splice($this->stack,count($this->stack)-2);  // reset stack from this action off

		//var_dump($this->search);
		
	//	var_dump($this->personid);
		if (!empty($this->personid)) 
		{	
			$person = $this->PersoonRepository->findByUid($this->personid);
			$this->return = 'detail';
			if ($typeadd=='addpartner')
			{
				// copy data from existing partner:
				$person["id_partner"]=$this->personid;
				$this->personid=0;
				if ($person['geslacht']=='man')	$person['geslacht']='vrouw';else $person['geslacht']='man';
				if ($person["burgerlijke_staat"]=="samenwonend")
				{
					$person['tussenvoegsel']=" ";
					$person['achternaam']=" ";
				}
				$person['partnernaam']=$person['naam'];
				
				$person['roepnaam']="";
				$person['voornamen']="";
				$person['emailadres']="";
				$person['geboortedatum']="";
				$person['geboorteplaats']="";
				$person['beroep']="";
				$person['gedoopt']="";
				$person['mobieltelnr']="";
				$person['id_vader']="";
				$person['id_moeder']="";
				$person["person_uid"]='';
			}
			if ($typeadd=='addchild')
			{
				// copy data from existing partner:
				$person['id_vader']="";
				$person['id_moeder']="";
				$person['vadernaam']='';
				$person['moedernaam']='';
				if ($person['geslacht']=='man')
				{
					$person["id_vader"]=$this->personid;
					$person['vadernaam']=$person['naam'];
					$person["id_moeder"]=$person["id_partner"];
					$person['moedernaam']=$person['partnernaam'];
				}
				if ($person['geslacht']=='vrouw')
				{
					$person["id_moeder"]=$this->personid;
					$person['moedernaam']=$person['naam'];
					$person["id_vader"]=$person["id_partner"];
					$person['vadernaam']=$person['partnernaam'];
				}
				$this->personid=0;
				$person['geslacht']='';
				$person['roepnaam']="";
				$person['voornamen']="";
				$person['emailadres']="";
				$person['geboortedatum']="";
				$person['geboorteplaats']="";
				$person['beroep']="";
				$person['gedoopt']="";
				$person['mobieltelnr']="";
				$person["person_uid"]='';
				$person["burgerlijke_staat"]='';
				$person["id_partner"]='';
				$person['partnernaam']='';
				$person['lid']='';
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction invoerdatum: '.$person['invoerdatum=']->format('Y-m-d')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

				$person['invoerdatum']=(new \DateTime())->format("Y-m-d");
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction invoerdatum: '.$person['invoerdatum=']->format('d-m-Y')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
				
			}
		}
		else 
		{
			$person=array();
			$person["postcode"]='';
			$person["huisnummer"]='';
			$person["straatnaam"]='';
			$person["woonplaats"]='';
			$person["land"]='';
			$person["id_partner"]='';
			$person["person_uid"]='';
			$person["deleted"]=0;
			$person["cleanteam"]="kan meedoen";
			$person["email_op_website"]='email tonen';
			$person["mobiel_op_website"]='mobiel tonen';
			$person["adres_op_website"]='adres tonen';
			$person["tel_op_website"]='telefoon tonen';
			$person["geboortedatum_op_intranet"]=1;
			$person["bezoeker"]='is bezoeker';
			$person["telefoonnr_vast"]='';
			$person["id_partner"]='';
			$person["burgerlijke_staat"]='';
		}
		$person["postcodeorg"]=$person["postcode"];
		$person["huisnummerorg"]=$person["huisnummer"];
		$person["straatnaamorg"]=$person["straatnaam"];
		$person["woonplaatsorg"]=$person["woonplaats"];
		$person["telefoonorg"]=$person["telefoonnr_vast"];
		$person["partnerorg"]=$person["id_partner"];
		$person["burgerlijke_staatorg"]=$person["burgerlijke_staat"];
		//$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
		$a_burgstaat=array('ongehuwd'=>'ongehuwd', 'gehuwd'=>'gehuwd', 'samenwonend'=>'samenwonend', 'gescheiden'=>'gescheiden', 'weduwnaar'=>'weduwnaar','weduwe'=>'weduwe','verbroken partnerschap'=>'verbroken partnerschap','overleden'=>'overleden');
		if (!empty($this->personid))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'geslacht: '.$persoon['geslacht']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			if ($person['geslacht']=='man') unset($a_burgstaat['weduwe']);
			if ($person['geslacht']=='vrouw') unset($a_burgstaat['weduwnaar']);
		}
		if ($person["burgerlijke_staat"]=='gehuwd')unset($a_burgstaat['verbroken partnerschap']);
		//$a_gebruiknaam=array('ja'=>'Partner naam','nee'=>'Eigen naam','beide'=>'Beide namen');

		//$extensionConfiguration['emailchangeaddresses']
		if ($personreg==1||!empty($this->personid) && ($person['id_vader']==$this->userid || $person['id_moeder']==$this->userid||$persoo['id_partner']==$this->userid|| $person['pid']==$this->userid))
			$ExtraPermissions=1; 
		else $ExtraPermissions=0;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'own userid: '.$this->userid.'; persreg: '.$this->search->getPersonreg().'; ExtraPermissions: '.$ExtraPermissions ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
//var_dump($ExtraPermissions);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'editAction person: '.urldecode(http_build_query($person,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'editAction stack: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction invoerdatum: '.$person['invoerdatum=']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		$assignedValues = [
			'person' => $person,
			'stack' => json_encode($this->stack),
            'currentPageId' => $this->pageId,
			'userid' => $this->userid,
			'emailchangeaddresses' => $this->extensionConfiguration['emailchangeaddresses'],
			'extraPermissions' => $ExtraPermissions,
			'userlid' => $this->frontendUser->user['lid'],
			'cleanteamReg'=> churchpersreg_div::Heeftpermissie("Cleanteams samenstellen"),
			'emailpersons' => churchpersreg_div::Heeftpermissie("Email personen"),
			'exportbrieven' => churchpersreg_div::Heeftpermissie("Export brieven aan persoon"),
			'scrijvenpersons' => churchpersreg_div::Heeftpermissie("Persoon schrijven"),
			'contactarchief' => churchpersreg_div::Heeftpermissie("ContactArchief lezen,ContactArchief schrijven"),
			'contactarchiefschrijven' => churchpersreg_div::Heeftpermissie("ContactArchief schrijven"),
			'extPath' => PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg')),
			'burgstaat'=> $a_burgstaat,
//			'gebrnaam'=> $a_gebruiknaam,
			'personreg' => (int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"),
			'return' => $this->return,
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();

    } 

    /**
     * action save
     *
     * @return void
     */
    public function saveAction()
    {
		if (! churchpersreg_div::Heeftpermissie("Persoon schrijven"))
		{	$ErrMsg='Geen rechten hiervoor'; return $ErrMsg;} 
//		$this->args=$this->request->getArguments();
		$Persoon=$this->args['person'];
		if (!empty($this->args['person']['person_uid']))$this->personid=$this->args['person']['person_uid'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SAVE PERSON args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$key=array_search('edit', array_column($this->stack, 'action'));
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person key:'.$key.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		if ($key)
		{
			
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person persoon: '.str_replace('=','="',urldecode(http_build_query($Persoon,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
	        $ErrMsg=$this->PersoonRepository->save($Persoon,$this->userid); 
			if (empty($ErrMsg))	
			{
				if (empty($this->personid))
				{
					array_splice($this->stack,$key);  // new: reset stack from this action off
					$this->personid=$Persoon['person_uid'];
				}
				else
				{ 
					array_splice($this->stack,$key+1);  // update: reset stack after this action off
					$this->personid='';
				}
			}
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person na opslag stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
/*		$assignedValues = [
			'stack' => json_encode($this->stack),
		];
		if (empty($this->personid)) $assignedValues['personid']=$Persoon['person_uid']; 
      //  $this->redirect('selectedList');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person REDIRECT to: '.$action."; assignedval:".str_replace('=','="',urldecode(http_build_query($assignedValues,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		//$this->redirect($action,'Persoon','churchpersreg',[ 'stack'=>json_encode($this->stack)]);
//		return new ForwardResponse('detail',null,null,$assignedValues)
        return (new ForwardResponse('detail'))
               ->withArguments($assignedValues);
*/
		return $ErrMsg;
    }


    /**
     * action delete
     *
     * @return void
     */
    public function deleteAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Persoon schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'DELETE PERSON args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		$key=count($this->stack)-1;
		$adresid=$this->args['adresid'];
		if(isset($this->args['huisgenoten']))$huisgenoten=$this->args['huisgenoten'];else $huisgenoten='';
		if(isset($this->args['verwijderreden']))$verwijderreden=$this->args['verwijderreden'];else $verwijderreden='';
		if(isset($this->args['removeall']))$removeall=$this->args["removeall"];else $removeall='';
		if ($verwijderreden=="2")$removeall=0;  // bij overlijden niet iedereen verwijderen
		$action="selectedList";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'DELETE PERSON verwijderreden: '.$verwijderreden.'; args:'.$this->args['verwijderreden']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

	    $ErrMsg=$this->PersoonRepository->delete($this->personid,$adresid,$huisgenoten,$verwijderreden,$removeall); 
//		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete person REDIRECT to: selectedList; stack:'.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		//$this->redirect($action,'Persoon','churchpersreg',[ 'stack'=>json_encode($this->stack)]);
        return (new ForwardResponse('selectedList'))
              ->withArguments(['stack'=>json_encode($this->stack)]);

    }
	


    /**
     * action contactInfoReg
     *
     * @return void
     */
    public function contactInfoRegAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("ContactArchief schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'contactInfoReg args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		if(isset($this->args['archiveid']))$id_archive=$this->args['archiveid'];else $id_archive='';
		if(isset($this->args['typecontent']))$typecontent=$this->args['typecontent'];else $typecontent='';
		if(isset($this->args['person_deleted']))$person_deleted=$this->args['person_deleted'];else $person_deleted='';
		$key=array_search('contactInfoReg', array_column($this->stack, 'action'));
//		if (!$key)$this->stack[]=array("action"=>"contactInfoReg",["archiveid"=>$id_archive,"typecontent"=>$typecontent,"person_uid"=>$this->personid]);
		if (!$key)$this->stack[]=array("action"=>"contactInfoReg",["archiveid"=>$id_archive,"typecontent"=>$typecontent,"personid"=>$this->personid]);
		else array_splice($this->stack,count($this->stack)-2);  // reset stack from this action off
		
		if (empty($id_archive)){
			$archive=array();
			$archive['uid']='';
			$archive['author']=$this->username;
			$archive['date']=date("Y-m-d");
			$archive['type_content']=$typecontent;
			$archive['id_person']=$this->personid;
			$archive['id_person_owner']=$this->userid;
			$archive['rtesavecontent']='';
		}
		else 
		{
			$archive=$this->PersoonRepository->findArchive($id_archive,$this->userid);
			$archive['rtesavecontent']=$this->rteSafe($archive['content']);
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'contactInfoReg  archive: '.urldecode(http_build_query($archive,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');

    	$assignedValues = [
			'archive' => $archive,
			'stack' => json_encode($this->stack),
			'contactarchiefreg' => (int)churchpersreg_div::Heeftpermissie("ContactArchief schrijven"),
			'contactconfidential' => (int)churchpersreg_div::Heeftpermissie("ContactArchief lezen vertrouwelijk"),
			'userid' => $this->userid,
			'persondeleted'=> $person_deleted,
			'personid' => $this->personid,
			'extPath' => PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg')),
        ];
        $this->view->assignMultiple($assignedValues); 
		$assetCollector = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\AssetCollector::class);
		$assetCollector->addJavaScript('nljs', 'EXT:parousiazoetermeer/Resources/Public/Javascript/jquery.nl.js',[], ['priority' => true]);
		$assetCollector->addJavaScript('cked', 'https://cdn.ckeditor.com/ckeditor5/33.0.0/classic/ckeditor.js',[], ['priority' => true]);
		return $this->htmlResponse();
		
    }
	
	
    /**
     * action contactInfoSave
     *
     * @return void
     */
    public function contactInfoSaveAction()
    {
		if (! churchpersreg_div::Heeftpermissie("ContactArchief schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
	//	$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'contactInfoSave args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if(isset($this->args['archive']))$archive=$this->args['archive'];else $archive='';
		$extPath= PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg'));

		$ErrMsg=$this->PersoonRepository->ContactInfoSave($archive,$extPath);
		$key=array_search('contactInfoReg', array_column($this->stack, 'action'));
		if ($key) 
		{	
			$action=$this->stack[$key-1]['action'];
			//array_splice($this->stack,$key);  // reset stack from this action off
		}
		else $action='detail';
		$assignedValues = [
			'stack' => json_encode($this->stack),
			//'personid' => $archive['id_person'],
		];

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save contactinfo REDIRECT to: '.$action."; assignedval:".str_replace('=','="',urldecode(http_build_query($assignedValues,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		//$this->redirect($action,'Persoon','churchpersreg',[ 'stack'=>json_encode($this->stack)]);
        return (new ForwardResponse($action))
              ->withArguments($assignedValues);
    }

    /**
     * action deleteArchive
     *
     * @return void
     */
    public function deleteArchiveAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("ContactArchief schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		$personid='';
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteArchive args: '.str_replace('=','="',urldecode(http_build_query($this->args,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$key=array_search('contactInfoReg', array_column($this->stack, 'action'));
		if ($key)
		{
			if(isset($this->args['archiveid']))$id_archive=$this->args['archiveid'];else $id_archive='';
			if(isset($this->args['id_person']))$personid=$this->args['id_person'];
			$action="detail";
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete id_archive : '.$id_archive."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
	        $ErrMsg=$this->PersoonRepository->deleteArchive($id_archive); 
			//if (empty($ErrMsg))	array_splice($this->stack,$key);  // reset stack from this action off
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete archive after delete error: '.$ErrMsg."; key:".$key."; stack:".str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 

		}
		else $action='detail';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete archive REDIRECT to: '.$action."; stack:".str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		//$this->redirect($action,'Persoon','churchpersreg',[ 'stack'=>json_encode($this->stack)]);
		$assignedValues = [
			'stack' => json_encode($this->stack),
			'personid' => $personid, 
		];
        return (new ForwardResponse($action))
              ->withArguments([ 'stack'=>json_encode($this->stack)]);
    }


	 /**
     * action export
     *
     * @return void
     */
    public function exportAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Export personen"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EXPORT persons args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$ErrMsg='';
		$key=array_search('selectedList', array_column($this->stack, 'action'));
		if ($key)
		{
			$action='selectedList';
			$this->search=$this->stack[$key]['search'];
			array_splice($this->stack,$key+1);  // reset stack from this action off
			
			
		//$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
		if ($this->search['peradres'])
		{
			$persons=$this->PersoonRepository->findSelection($this->search);
			// expand huisgenoten to an array:
			for ($i=0;$i<count($persons);$i++)
			{
				$huisgenot=explode(', ',$persons[$i]['huisgenoten']);
				$huisgenoten=array();
				for ($j=0;$j<count($huisgenot);$j++) 
				{
					$huisg=explode('#',$huisgenot[$j]);
					$huisgenoten[]=$huisg[1];
				}
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList huisgenoten: '.urldecode(http_build_query($huisgenoten,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
				$persons[$i]['huisgenoten']=implode(", ",$huisgenoten);
			}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList persons: '.urldecode(http_build_query($persons,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		}
		else
			$persons=$this->PersoonRepository->findExportPersons($this->search);
		}
		$exportfilenaam=churchpersreg_div::ExportToXlsx($persons,"exportpersonen",'','',$this->username);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (empty($action)) $action='searchForm';

		$assignedValues = [
			'downloadfile' => "https://".$_SERVER['HTTP_HOST']."/".$exportfilenaam,
			'stack' => json_encode($this->stack),
			'return' => $action,
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Export action:'.$action.'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
        return (new ForwardResponse($action))
              ->withArguments($assignedValues);
	}
	
	 /**
     * action adressenlijst
     *
     * @return void
     */
    public function adressenlijstAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Export adressenlijst"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		$exportfilenaam=$this->GetList('adressenlijst');
		$assignedValues = [
			'downloadfile' => "https://".$_SERVER['HTTP_HOST']."/".$exportfilenaam,
			'home' => "https://".$_SERVER['HTTP_HOST']."//index.php?id=homepage",
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Export action:'.$action.'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();

	}

	 /**
     * action adressenlijst
     *
     * @return void
     */
    public function ledenlijstAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Export adressenlijst"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		$exportfilenaam=$this->GetList('ledenlijst');
		$assignedValues = [
			'downloadfile' => "https://".$_SERVER['HTTP_HOST']."/".$exportfilenaam,
			'home' => "https://".$_SERVER['HTTP_HOST']."//index.php?id=homepage",
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Export action:'.$action.'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();

	}
	
	
	 /**
     * action requestSend
     *
     * @return void
     */
    public function requestSendAction(): ResponseInterface
    {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction args:'.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (isset($this->args["errorkey"]))$errorkey=$this->args["errorkey"];
		$assignedValues = array();
		if (! churchpersreg_div::Heeftpermissie("Smoelenboek lezen"))  //everyone logged in has this permission
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		$typereq=$this->settings['typerequest'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction errorkey:'.$errorkey."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (empty($errorkey))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction typerequest: '.$typereq.'; lid:'.$this->frontendUser->user['gedoopt']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			if ($typereq=="lidmaatschap" && $this->frontendUser->user['lid']=="is lid")
				$errorkey="persoon.msgallid";
			elseif ($typereq=="lidmaatschap" && $this->frontendUser->user['gedoopt']!="is gedoopt")
				$errorkey="persoon.msgdopen";
			elseif ($typereq=="doop" && $this->frontendUser->user['gedoopt']=="is gedoopt")
				$errorkey="persoon.msgalgedoopt";
			else
			{
				// check al aanvraag ingediend:
				$aanvragen=$this->PersoonRepository->findSelectionAanvraag(['uidpersoon' => $this->userid,'typerequest'=> $typereq]);
				if (!empty($aanvragen)) $errorkey="persoon.msgalaanvraag";
				else
				{
					// correct request:
					if ($typereq=="lidmaatschap" or $typereq=="doop")
					{
						$person=$this->PersoonRepository->findByUid($this->userid);
						$geboortedatum = new \DateTime($person['geboortedatum']);
					}
		
					if ($typereq=="lidmaatschap" )
					{
						// get data from corresponding persons:
						// check age:
						$invoerdatum= new \DateTime($person['invoerdatum']);
						$age18= new \DateTime('- 18 years');
						$months5= new \DateTime('-5 months');
						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction leeftijd: '.$geboortedatum->format("d-m-Y").'; in db: '.$person['geboortedatum'].'; age18: '.$age18->format("d-m-Y")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction in gemeente: '.$invoerdatum->format("d-m-Y").'; in db: '.$person['invoerdatum'].'; 5 months: '.$months5->format("d-m-Y")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
						if ($geboortedatum > $age18)
							$errorkey="persoon.msgtejong";
						elseif ($invoerdatum > $months5)
							$errorkey="persoon.msgtekort";
						if (empty($errorkey))
						{
							if ($person['burgerlijke_staat']=="gehuwd" && !empty($person['id_partner']))
							{
								$partner=$this->PersoonRepository->findByUid($person['id_partner']);
								//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction partner: '.urldecode(http_build_query($partner,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
								if ($partner['lid']!='is lid' && $partner['gedoopt']=='is gedoopt' && (new \DateTime($partner['geboortedatum'])) <= $age18 && (new \DateTime($partner['invoerdatum'])) <= $months5)
									$assignedValues['partner'] = $person['partnernaam'];
									$assignedValues['partner_uid'] = $person['id_partner'];
							}
							$assignedValues['person']=$person;
						}
					}
					elseif ($typereq=="doop")
					{
						$age14= new \DateTime('- 14 years');
						if ($geboortedatum > $age14)
							$errorkey="persoon.msgtejongdoop";
						if (empty($errorkey))
						{
							if ($person['burgerlijke_staat']=="gehuwd" && !empty($person['id_partner']))
							{
								$partner=$this->PersoonRepository->findByUid($person['id_partner']);
								//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction partner: '.urldecode(http_build_query($partner,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
								if ($partner['lid']!='is lid' && $partner['gedoopt']!='is gedoopt' && (new \DateTime($partner['geboortedatum'])) <= $age14)
									$assignedValues['partner'] = $person['partnernaam'];
									$assignedValues['partner_uid'] = $person['id_partner'];
							}
							$assignedValues['person']=$person;
						}
						$assignedValues['person']=$person;
					}

				}
			}
		}
		$assignedValues['errorkey']=$errorkey;
		$assignedValues['typerequest']=$typereq;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'requestSendAction:'.$action.'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();

	}

    /**
     * action saveRequest
     *
     * @return void
     */
    function saveRequestAction(): ResponseInterface
    {
//		$this->args=$this->request->getArguments();
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveRequestAction args:'.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->personid=$this->args['person_uid'];
		$partneraanvraag=$this->args['partneraanvraag'];
		$person=$this->PersoonRepository->findByUid($this->personid);
		$typerequest=$this->args['typerequest'];
		// check if aanvraag al ingediend
		$aanvragen=$this->PersoonRepository->findSelectionAanvraag(['uidpersoon' => $this->personid,'typerequest'=> $typerequest]);
		if (!empty($aanvragen)) 
		{
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveRequestAction gevonden aanvragen:'.urldecode(http_build_query($aanvragen,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			$errorkey="persoon.msgalaanvraag";
			$assignedValues=[
				'errorkey' => $errorkey,
				'typerequest'=> $typerequest,
			];

        return (new ForwardResponse("requestSend"))
              ->withArguments($assignedValues);
		}

	    $assignedValues = [
			'person' => $person,
			'partneraanvraag' => $partneraanvraag,
      		];
		if (!empty($partneraanvraag))
		{
			$partner=$this->PersoonRepository->findByUid($partneraanvraag);
			$assignedValues['partner']=$partner;
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveRequestAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person persoon: '.str_replace('=','="',urldecode(http_build_query($Persoon,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
	    $ErrMsg=$this->PersoonRepository->saveRequest($this->personid,$typerequest,$partneraanvraag); 
		//if (!empty($partneraanvraag))$this->PersoonRepository->saveRequest($partneraanvraag,$this->args['typerequest'],$this->personid); 
		
		//if (empty($ErrMsg))	
		
		// send email to handler of request:
		 
		
		$email=['from'=>new Address($GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'],$person['naam']),
		'to'=>new Address($this->settings['destinationemail'], $this->settings['destinationname'])];
		if (!empty($this->settings['destinationCC']))
		   $email['cc']= new Address($this->settings['destinationCC'], $this->settings['destinationCCname']);
		$email['reply']= new Address($person['emailadres'], $person['naam']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveRequestAction email: '.urldecode(http_build_query($email,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$assignedValues['typerequest']=$this->settings['typerequest'];

		$template=ucfirst($assignedValues['typerequest']).'Aanvraag';
		$this->SendEmailFluid($email,$assignedValues,$template);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveRequestAction confirmation mail person: '.urldecode(http_build_query($person,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		
		// send confirmation email:
	    $assignedValues = [
		    'namesender' => $this->settings['destinationname'],
			'voornaam' => $person['roepnaam'],
			'naam' => $person['naam'],
			'typerequest' => $this->args['typerequest'],
   		];

		$email=['from'=>new Address($GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'],$this->settings['destinationname']),
		'reply'=>new Address($this->settings['destinationemail'], $this->settings['destinationname']),
		'to'=>new Address($person['emailadres'],$person['naam'])];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveRequestAction confirmation mail voornaam: '.$person['voornaam'].'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->SendEmailFluid($email,$assignedValues,'AanvraagBevestiging');

		// return:
		if ($this->settings['typerequest']=="lidmaatschap")$assignedValues['errorkey']="persoon.msgthankyourlid";
		if ($this->settings['typerequest']=="doop")$assignedValues['errorkey']="persoon.msgthankyourdoop";
		$assignedValues['errorkey']="persoon.msgthankyourequest";
		$assignedValues['typerequest']=$this->settings['typerequest'];
        return (new ForwardResponse("requestSend"))
              ->withArguments($assignedValues);


		//header('Location: '.GeneralUtility::locationHeaderUrl('index.php'));
		//exit;
    }
	
    /**
     * action searchUpdatelog
     *
     * @return void
     */
    public function searchUpdatelogAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))  //everyone logged in has this permission
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'searchAanvragen args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (isset($this->args['searchupdatelog']))$this->search=$this->args['searchupdatelog'];
		if (isset($this->args['stack']))$this->stack=$this->args['stack'];
		if (!is_null($this->stack) and is_array($this->stack))
		{
			$key=array_search('selectedListUpdatelogs', array_column($this->stack, 'action'));
			if ($key and isset($this->stack[$key]["searchupdatelog"]))$this->search=$this->stack[$key]["searchupdatelog"];
		}
		
        if (is_null($this->search)) 
		{
            $this->search = array();
        }
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'searchAanvragen search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		// reinitialize stack:
		$this->stack=array(array("action"=>"searchUpdatelog","searchupdatelog"=>$this->search));
//var_dump($this->search);
    	$assignedValues = [
			'searchupdatelog' => $this->search,
			'downloadfile' => $this->downloadfile,
			'stack' => json_encode($this->stack),
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

    /**
     * action selectedListUpdatelogs
     *
     * @return void
     */
    public function selectedListUpdatelogsAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))die("You don't have the privilege to perform this action");
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListUpdatelogsAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if(isset($this->args['searchupdatelog']))$this->search=$this->args['searchupdatelog'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListUpdatelogs search : '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListUpdatelogs stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (!is_array($this->stack))$this->stack=[array('action'=>"searchUpdatelog","searchupdatelog"=>$this->search),array("action"=>"selectedListUpdatelogs","search"=>$this->search)];
		else
		{
			$key=array_search('selectedListUpdatelogs', array_column($this->stack, 'action'));
			if (!$key)$this->stack[]=array("action"=>"selectedListUpdatelogs","searchupdatelog"=>$this->search);
			else 
			{
				$this->search=$this->stack[$key]['searchupdatelog'];
				array_splice($this->stack,$key+1);  // reset stack from this action off
			}
		}
        if (is_null($this->search)) 
		{
            $this->search = array();
        }

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListUpdatelogs search nw: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListUpdatelogs stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$updatelogs=$this->PersoonRepository->findSelectionUpdatelog($this->search);
//		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
    	$assignedValues = [
			'updatelogs' => $updatelogs,
			'stack' => json_encode($this->stack),
			'search' => $this->search,
			'downloadfile' => $this->downloadfile,
            'currentPageId' => $this->pageId,
			'userid' => $this->userid,
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

    /**
     * action detailupdatelog
     *
     * @return void
     */
    public function detailUpdatelogAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailupdatelogAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//$this->search=$this->args['detailupdatelog'];
//		$this->args=$this->request->getArguments();
		$requestid=$this->args['f_uid'];
		$joinrequest=$this->PersoonRepository->findUpdatelogByUid($requestid);
		$assignedValues = [
			'updatelog' => $joinrequest,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'writepermission' =>(int)churchpersreg_div::Heeftpermissie("Persoon schrijven"),
			'extPath' => PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg')),
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction personid: '.$this->personid.'; args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
	}
	


    /**
     * action searchAanvragen
     *
     * @return void
     */
    public function searchAanvragenAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))  //everyone logged in has this permission
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'searchAanvragen args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (!empty($this->stack))
		{
			$key=array_search('selectedListAanvragen', array_column($this->stack, 'action'));
			if ($key)$this->search=$this->stack[$key]["searchaanvraag"];
		}
		
        if (empty($this->search)) 
		{
            $this->search = array();
			$this->search['typerequest']="aansluiting";
			$this->search['openonly']=1;
        }
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'searchAanvragen search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		// reinitialize stack:
		$this->stack=array(array("action"=>"searchAanvragen","searchaanvraag"=>$this->search));
//var_dump($this->search);
    	$assignedValues = [
			'searchaanvraag' => $this->search,
			'downloadfile' => $this->downloadfile,
			'stack' => json_encode($this->stack),
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

    /**
     * action selectedListAanvragen
     *
     * @return void
     */
    public function selectedListAanvragenAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))die("You don't have the privilege to perform this action");
//		$this->args=$this->request->getArguments();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListAanvragenAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (isset($this->args['searchaanvraag']))$this->search=$this->args['searchaanvraag'];else $this->search='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListAanvragen search : '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListAanvragen stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		if (!is_array($this->stack))$this->stack=[array('action'=>"searchAanvragen","searchaanvraag"=>$this->search),array("action"=>"selectedListAanvragen","search"=>$this->search)];
		else
		{
			$key=array_search('selectedListAanvragen', array_column($this->stack, 'action'));
			if (!$key)$this->stack[]=array("action"=>"selectedListAanvragen","searchaanvraag"=>$this->search);
			else 
			{
				$this->search=$this->stack[$key]['searchaanvraag'];
				array_splice($this->stack,$key+1);  // reset stack from this action off
			}
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListAanvragen search nw: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'selectedListAanvragen stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		$aanvragen=$this->PersoonRepository->findSelectionAanvraag($this->search);
//		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchpersreg');
    	$assignedValues = [
			'aanvragen' => $aanvragen,
			'stack' => json_encode($this->stack),
			'search' => $this->search,
			'downloadfile' => $this->downloadfile,
            'currentPageId' => $this->pageId,
			'userid' => $this->userid,
        ];
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

    /**
     * action detailaanvraag
     *
     * @return void
     */
    public function detailaanvraagAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailaanvraagAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//$this->search=$this->args['detailaanvraag'];
//		$this->args=$this->request->getArguments();
		if(isset($this->args['f_uid']))$requestid=$this->args['f_uid'];else $requestid='';
		if(isset($this->args['f_type']))$ftype=$this->args['f_type'];else $ftype='';
		if ($ftype=='a')$typerequest="aansluiting";
		elseif ($ftype=='d')$typerequest="doop";
		else $typerequest="lidmaatschap";
		if ($requestid)
		{
			//$deleteble=false;
			$now = new \DateTime("-10 days");
			
			$assignedValues = [
				'stack' => json_encode($this->stack),
				'userid' => $this->userid,
				'writepermission' =>(int)churchpersreg_div::Heeftpermissie("Persoon schrijven"),
				'extPath' => PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchpersreg')),
	        ];
			if ($typerequest=='aansluiting')
			{	
				$joinrequest=$this->PersoonRepository->findAanvraagByUid($requestid);
				$assignedValues['typerequest'] = 'aansluiting';
				$aanvraagDate=new \DateTime();
				$aanvraagDate->setTimestamp($joinrequest['crdate']);
			}
			else 
			{
				$joinrequest=$this->PersoonRepository->findRequestByUid($requestid,$typerequest);
				$person=$this->PersoonRepository->findByUid($joinrequest['uidpersoon']);
				$assignedValues['person'] = $person;
				$assignedValues['typerequest'] = $joinrequest['typerequest'];
				$aanvraagDate=new \DateTime($joinrequest['datumaanvraag']);
				if (!empty($joinrequest['uidpartner']))
				{
					$partner=$this->PersoonRepository->findByUid($joinrequest['uidpartner']);
					$assignedValues['partner'] = $partner;
      			}
			}
			if ($assignedValues['writepermission'] and (empty($joinrequest['invoerdatum']) or $joinrequest['invoerdatum']=='0000-00-00') and $aanvraagDate<$now ) $deleteble=true; else $deleteble=false;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction ingevoerd: '.$joinrequest['invoerdatum']."; deleteble: ".$deleteble."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			
			$assignedValues['deleteble'] = $deleteble;
			$assignedValues['aanvraag'] = $joinrequest;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction personid: '.$this->personid.'; assignedValues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
			$this->view->assignMultiple($assignedValues); 
		}
		else
		{
			$assignedValues = [
				'personid' => $this->personid,
				'stack' => json_encode($this->stack),
			];
	        return (new ForwardResponse("detail"))
              ->withArguments($assignedValues);
		}
		return $this->htmlResponse();
	}
	
    /**
     * action saveaanvraag
     *
     * @return void
     */
    public function saveAanvraagAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaanvraagAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//$this->search=$this->args['detailaanvraag'];
//		$this->args=$this->request->getArguments();
		if(isset($this->args['uid']))$requestid=$this->args['uid'];else $requestid='';
		if(isset($this->args['typerequest']))$typerequest=$this->args['typerequest'];else $typerequest='';
		if(isset($this->args['id_bezoeker']))$id_bezoeker=$this->args['id_bezoeker'];else $id_bezoeker='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveaanvraagAction id_bezoeker: '.$id_bezoeker."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
        $ErrMsg=$this->PersoonRepository->saveAanvraag($requestid,$id_bezoeker,$typerequest);
      //  $this->redirect('selectedList');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person REDIRECT to: '.$action."; assignedval:".str_replace('=','="',urldecode(http_build_query($assignedValues,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		//$this->redirect($action,'Persoon','churchpersreg',[ 'stack'=>json_encode($this->stack)]);
        return (new ForwardResponse("selectedListAanvragen"));
	}

		    /**
     * action deleteAanvraag
     *
     * @return void
     */
    function deleteAanvraagAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Persoon lezen,Persoon schrijven"))die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteAanvraagAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt');
		//$this->search=$this->args['detailaanvraag'];
//		$this->args=$this->request->getArguments();
		if(isset($this->args['uid']))$requestid=$this->args['uid'];else $requestid='';
		if(isset($this->args['typerequest']))$typerequest=$this->args['typerequest'];else $typerequest='';
        $ErrMsg=$this->PersoonRepository->deleteAanvraag($requestid,$typerequest);
      //  $this->redirect('selectedList');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save person REDIRECT to: '.$action."; assignedval:".str_replace('=','="',urldecode(http_build_query($assignedValues,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Controller/debug.txt'); 
		//$this->redirect($action,'Persoon','churchpersreg',[ 'stack'=>json_encode($this->stack)]);
        return (new ForwardResponse("selectedListAanvragen"));
		
	}

	


	//
	//	Send email fluid to user 
	//

	function SendEmailFluid($email,$assignedValues,$templatename){
		// Prepare and send the message
		$mail= new FluidEmailReal();
		$mail
		   // Give the message a subject
		   ->from($email['from'])
		   // Give it a body
		   ->format('html')
		   ->to($email['to'])
		   ->setTemplate($templatename)
		   ->assignMultiple($assignedValues);
		if (!empty($email['cc']))
		   $mail->cc($email['cc']);
		  if (!empty($email['reply']))
  		   	$mail->replyTo($email['reply']);

		$mail->send();
	}

	
	/**
	 * Method GetList: get array with addresses to export
	 *
	 * @param	string		$type: addressenlijst, ledenlijst
	 */
	function GetList($type = "ledenlijst"){
		$this->aExport=$this->PersoonRepository->FindListPersons($type);
		$exportnaam="export".$type;
		$exportfilenaam=churchpersreg_div::ExportToXlsx($this->aExport,$exportnaam,$type,array("Naam","Adres","Postcode","Woonplaats","Land","Tel.nummer","E-mailadres"),$this->username);
		return $exportfilenaam;
	} 
	
	
	function rteSafe($strText) 
	{
		//returns safe code for preloading in the RTE
		$tmpString = $strText;
		
		//convert all types of single quotes
		$tmpString = str_replace(chr(145), chr(39), $tmpString);
		$tmpString = str_replace(chr(146), chr(39), $tmpString);
		$tmpString = str_replace("'", "&#39;", $tmpString);
		
		//convert all types of double quotes
		$tmpString = str_replace(chr(147), chr(34), $tmpString);
		$tmpString = str_replace(chr(148), chr(34), $tmpString);
	//	$tmpString = str_replace("\"", "\"", $tmpString);
		
		//replace carriage returns & line feeds
		$tmpString = str_replace(chr(10), " ", $tmpString);
		$tmpString = str_replace(chr(13), " ", $tmpString);
		
		return $tmpString;
	}

	 /**
     * Returns a random string needed to generate a fileName for the queue.
     *
     * @param int $count
     *
     * @return string
     */
    protected function getRandomString(int $count): string
    {
        // This string MUST stay FS safe, avoid special chars
        $base = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-';
        $ret = '';
        $strlen = strlen($base);
        for ($i = 0; $i < $count; ++$i) {
            $ret .= $base[((int)random_int(0, $strlen - 1))];
        }
        return $ret;
    }
	
}
