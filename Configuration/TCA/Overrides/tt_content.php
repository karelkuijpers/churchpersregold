<?php
defined('TYPO3') or die();

/***************
 * Plugin
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Churchpersreg',
    'Persoon',
    'Church member administration'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['churchpersreg_persoon'] = 'recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['churchpersreg_persoon'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('churchpersreg_persoon',
    'FILE:EXT:churchpersreg/Configuration/FlexForms/flexform_persoon.xml'); 

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToInsertRecords('persoon');
