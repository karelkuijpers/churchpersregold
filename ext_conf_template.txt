# Records
###########################
# cat=email; type=string; label=e-mail address for changing your personal data
emailchangeaddresses= hivasthotmail.com

# cat=sendemail; type=string; label=transport_smtp_user0 for swift mailer with usernamel#$#password
transport_smtp_user0 = 
# cat=sendemail; type=string; label=transport_smtp_user1 for swift mailer with usernamel#$#password
transport_smtp_user1= 

# cat=sendemail; type=string; label=transport_smtp_user2 for swift mailer with usernamel#$#password
transport_smtp_user2= 

# cat=sendemail; type=string; label=transport_smtp_user3 for swift mailer with usernamel#$#password
transport_smtp_user3= 

# cat=sendemail; type=string; label=transport_smtp_user4 for swift mailer with usernamel#$#password
transport_smtp_user4= 

# cat=salutation; type=string; label=appreciation of receiver: value, ...
appreciation=Beste,Lieve,Geachte,Geliefde, 
# cat=salutation; type=string; label=default appreciation
appreciationdefault=Beste

# cat=salutation; type=string; label=relationship to receiver: value&male value&female value, ...
relationship=broeder/zuster&broeder&zuster,heer/mevrouw&heer&mevrouw,
# cat=salutation; type=string; label=default relationship
relationshipdefault= 

# cat=salutation; type=options[first name=firstname,full name=fullname,last name=lastname,]; label=default name
namedefault= firstname

# cat=tasks; type=string; label=page number for showing hierarchy of ministries
pagenbrministrytree = 301


